cmake_minimum_required(VERSION 3.10)
project(Totoro)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DILOUSESTL -fPIC")

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

find_package(Cplex)
find_package(Boost 1.67 REQUIRED)

add_subdirectory(src)

add_executable(Hyperstories src/main.cpp)
target_link_libraries(Hyperstories stories)

