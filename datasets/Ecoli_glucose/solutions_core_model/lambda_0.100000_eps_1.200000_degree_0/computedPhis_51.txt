F_R_ACALD 	 1.2
F_R_ACKr 	 1.2
B_R_ACONTa 	 0.378523
F_R_ACONTb 	 0.821478
F_R_ADK1 	 0.998176
R_AKGDH 	 2.85336
F_R_ATPS4r 	 4.3708
R_BIOMASS_Ecoli_core_w_GAM 	 0.107101
R_CS 	 0.381477
R_CYTBD 	 1.90239
F_R_ENO 	 4.26014
B_R_FBA 	 1.02453
R_FRD7 	 2.65707
B_R_FUM 	 3.53707
B_R_G6PDH2r 	 1.017
F_R_GAPD 	 1.07929
F_R_GLUDy 	 1.2
R_GLUSy 	 1.46461
F_R_ICDHyr 	 1.47691
R_ICL 	 0.544563
F_R_LDH_D 	 1.2
R_MALS 	 0.544563
B_R_MDH 	 3.48051
R_NADH16 	 4.55947
R_PDH 	 0.127434
R_PFK 	 2.64747
B_R_PGI 	 0.794956
B_R_PGK 	 2.02036
R_PGL 	 0.183
B_R_PGM 	 3.06014
R_PPC 	 3.25771
R_PYK 	 2.00683
F_R_RPE 	 1.52302
F_R_RPI 	 0.323016
B_R_SUCOAS 	 4.05336
F_R_TALA 	 1.98084
F_R_TKT1 	 0.78084
F_R_TKT2 	 1.94218
F_R_TPI 	 0.175467
