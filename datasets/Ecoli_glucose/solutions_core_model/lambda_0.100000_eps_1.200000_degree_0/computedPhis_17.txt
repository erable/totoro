F_R_ACALD 	 1.2
F_R_ACKr 	 1.2
F_R_ACONTb 	 1.2
F_R_ADK1 	 0.931
R_AKGDH 	 2.85336
F_R_ATPS4r 	 4.77973
R_BIOMASS_Ecoli_core_w_GAM 	 0.107101
R_CS 	 0.76
R_CYTBD 	 2.45666
F_R_ENO 	 4.02694
B_R_FBA 	 0.961375
R_FRD7 	 2.27855
B_R_FUM 	 3.15855
B_R_G6PDH2r 	 1.017
F_R_GAPD 	 1.20561
R_GLCpts 	 0.0631585
F_R_GLUDy 	 1.2
R_GLUSy 	 1.46461
F_R_ICDHyr 	 1.47691
R_ICL 	 0.923086
F_R_LDH_D 	 1.2
R_MALS 	 0.332061
B_R_MDH 	 3.31449
R_NADH16 	 4.73521
R_PFK 	 2.71063
B_R_PGI 	 0.731797
B_R_PGK 	 1.78716
R_PGL 	 0.183
B_R_PGM 	 2.82694
R_PPC 	 3.14111
B_R_PTAr 	 0.491889
R_PYK 	 1.82707
F_R_RPE 	 1.52302
F_R_RPI 	 0.323016
B_R_SUCOAS 	 4.05336
F_R_TALA 	 1.98084
F_R_TKT1 	 0.78084
F_R_TKT2 	 1.94218
F_R_TPI 	 0.238625
