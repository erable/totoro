M_glc__D_e	0
M_gln__L_c	-0.888
M_gln__L_e	0
M_glu__L_c	-3.908
M_glu__L_e	0
M_glx_c	0
M_h2o_c	1.14629
M_h2o_e	0
M_h_c	0
M_h_e	0
M_icit_c	1.68
M_lac__D_c	0
M_lac__D_e	0
M_mal__L_c	0.341
M_mal__L_e	0
M_nad_c	2.90619
M_13dpg_c	0
M_nadh_c	-2.90619
M_nadp_c	-0.201688
M_nadph_c	0.201688
M_nh4_c	4.46982
M_nh4_e	0
M_2pg_c	0
M_o2_c	-5.43476
M_3pg_c	-0.333115
M_o2_e	0
M_oaa_c	-0.025433
M_6pgc_c	-0.2745
M_pep_c	-1.06
M_6pgl_c	0.869353
M_pi_c	3.57341
M_ac_c	-5.9952e-13
M_pi_e	0
M_pyr_c	-9.5943
M_pyr_e	0
M_q8_c	0
M_ac_e	0
M_q8h2_c	0
M_acald_c	0
M_r5p_c	-0.199891
M_acald_e	0
M_ru5p__D_c	0.2745
M_accoa_c	5
M_s7p_c	0.0779144
M_succ_c	0
M_acon_C_c	5.09814e-13
M_succ_e	0
M_actp_c	5.9952e-13
M_succoa_c	0
M_adp_c	-0.269
M_xu5p__D_c	0.158298
M_akg_c	2.87576
M_akg_e	0
M_amp_c	1.449
M_atp_c	-1.18
M_cit_c	1.68
M_co2_c	3.71561
M_co2_e	0
M_coa_c	-5
M_dhap_c	0.7027
M_e4p_c	0
M_etoh_c	0
M_etoh_e	0
M_f6p_c	-0.252
M_fdp_c	-0.7027
M_for_c	0
M_for_e	0
M_fru_e	0
M_fum_c	0
M_fum_e	0
M_g3p_c	0.593592
M_g6p_c	-0.915
M_biomass_c	1.11335
