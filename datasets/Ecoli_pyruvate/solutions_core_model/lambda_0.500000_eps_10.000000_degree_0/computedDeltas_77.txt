M_glc__D_e	0
M_gln__L_c	-1.464
M_gln__L_e	0
M_glu__L_c	-4.232
M_glu__L_e	0
M_glx_c	10
M_h2o_c	1.65744
M_h2o_e	0
M_h_c	9.38594
M_h_e	0
M_icit_c	1.68
M_lac__D_c	0
M_lac__D_e	0
M_mal__L_c	0.571
M_mal__L_e	0
M_nad_c	0
M_13dpg_c	-0.0865796
M_nadh_c	0
M_nadp_c	0
M_nadph_c	0
M_nh4_c	7.16
M_nh4_e	0
M_2pg_c	0
M_o2_c	-20.9094
M_3pg_c	0
M_o2_e	0
M_oaa_c	9.4525
M_6pgc_c	-0.2955
M_pep_c	-1.06
M_6pgl_c	0
M_pi_c	0
M_ac_c	10
M_pi_e	0
M_pyr_c	-50
M_pyr_e	0
M_q8_c	-1.77636e-15
M_ac_e	0
M_q8h2_c	1.77636e-15
M_acald_c	0
M_r5p_c	0
M_acald_e	0
M_ru5p__D_c	0.2955
M_accoa_c	0
M_s7p_c	3.30248
M_succ_c	-9.7522e-13
M_acon_C_c	2.48048
M_succ_e	0
M_actp_c	0
M_succoa_c	0
M_adp_c	-0.269
M_xu5p__D_c	3.30248
M_akg_c	5.696
M_akg_e	0
M_amp_c	1.449
M_atp_c	-1.18
M_cit_c	1.68
M_co2_c	10
M_co2_e	0
M_coa_c	0
M_dhap_c	0
M_e4p_c	0
M_etoh_c	0
M_etoh_e	0
M_f6p_c	-0.252
M_fdp_c	-0.7229
M_for_c	0
M_for_e	0
M_fru_e	0
M_fum_c	0.272
M_fum_e	0
M_g3p_c	0
M_g6p_c	-1.045
M_biomass_c	0
