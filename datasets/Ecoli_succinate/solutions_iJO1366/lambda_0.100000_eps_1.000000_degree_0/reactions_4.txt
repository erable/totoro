Reactions	ColorReaction
R_DM_5drib_c	10
R_DM_mththf_c	10
R_DM_oxam_c	10
R_5DOAN	10
R_ACACT1r	-10
R_ACACT2r	-10
R_ACACT3r	-10
R_ACACT4r	-10
R_ACACT5r	-10
R_ACACT6r	-10
R_ACACT7r	-10
R_ACACT8r	10
R_ACALD	10
R_ACKr	10
R_ACOAD1f	10
R_ACOAD2f	10
R_ACOAD3f	10
R_ACOAD4f	10
R_ACOAD5f	10
R_ACOAD6f	10
R_ACOAD7f	10
R_ACOAD8f	10
R_ACONTa	-10
R_ACONTb	-10
R_AHCYSNS	10
R_AICART	10
R_AIRC2	10
R_AIRC3	-10
R_ADD	10
R_ADK1	10
R_ADNK1	10
R_ADNt2pp_copy2	10
R_ADSL2r	10
R_ALLTAMH	10
R_ALLTN	10
R_ASNS2	10
R_ASPK	10
R_ASPTA	-10
R_CBMKr	-10
R_AST	10
R_ATPS4rpp	10
R_CITL	10
R_CLPNH180pp	10
R_CYTBO3_4pp	10
R_DADNt2pp	10
R_ARGORNt7pp	10
R_DRPA	10
R_DHPTDCs2	10
R_ASAD	-10
R_ECOAH5	-10
R_ECOAH6	-10
R_ECOAH7	-10
R_ECOAH8	-10
R_ENO	-10
R_ECOAH1	-10
R_ECOAH2	-10
R_ECOAH3	-10
R_ECOAH4	-10
R_F6PA	-10
R_FLDR2	10
R_FTHFLi	10
R_FUM	10
R_FACOAL180t2pp	10
R_G3PD2	10
R_FBA	-10
R_FBP	10
R_GHMT2r	10
R_GLNS	10
R_GAPD	-10
R_GLUDy	-10
R_GLUPRT	10
R_GART	10
R_FE3Ri	10
R_GLYAT	-10
R_GLYC3Pt6pp	10
R_GLYCDx	10
R_GUAD	10
R_GUAt2pp	10
R_H2Otpp	-10
R_GLYCtpp	-10
R_HACD1	-10
R_HACD2	-10
R_HACD3	-10
R_HACD4	-10
R_HACD5	-10
R_HACD6	-10
R_HACD7	-10
R_HACD8	-10
R_HCO3E	10
R_HCYSMT	10
R_GND	10
R_GPDDA4pp	10
R_HSDy	-10
R_HSK	10
R_FESD1s	10
R_HXAND	10
R_FESR	10
R_IPPMIb	-10
R_IPPS	10
R_ICDHyr	-10
R_IMPC	-10
R_LPLIPAL1A180pp	10
R_METAT	10
R_MAN6PI	10
R_MDH	10
R_LPLIPAL1G180pp	10
R_MOX	-10
R_MTHFC	10
R_MTHFD	10
R_MTHTHFSs	10
R_NADH16pp	10
R_NADH17pp	10
R_NTD11	10
R_URIC	10
R_OXAMTC	10
R_PGCD	10
R_PGI	10
R_PGK	10
R_PGM	10
R_PFL	10
R_PLIPA2A180pp	10
R_PLIPA2G180pp	10
R_PHETA1	10
R_PIt2rpp	10
R_POR5	-10
R_PPCK	10
R_PPK	-10
R_PPM2	10
R_PPS	10
R_PRAGSr	10
R_PRAIS	10
R_PRASCSi	10
R_PRFGS	10
R_PRPPS	10
R_RPE	-10
R_RPI	-10
R_SADH	10
R_PSERT	10
R_PSP_L	10
R_SERASr	-10
R_PTAr	-10
R_PTRCORNt7pp	-10
R_SGDS	10
R_SGSAD	10
R_PTRCt2pp	10
R_QMO3	10
R_PUNP1	-10
R_PUNP2	10
R_PUNP3	10
R_PUNP4	10
R_PUNP5	10
R_SOTA	10
R_R15BPK	10
R_R1PK	10
R_SPODM	10
R_THRD	10
R_THRS	10
R_SUCDi	10
R_SUCOAS	10
R_TKT1	-10
R_TKT2	-10
R_TALA	-10
R_TPI	-10
R_THD2pp	10
R_TRPt2rpp	-10
R_VALTA	10
R_XAND	10
R_RHCCE	10
R_URDGLYCD	10
