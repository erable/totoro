B_R_ACONTa 	 3.32
F_R_ACONTb 	 1.68
F_R_ATPS4r 	 30.0699
R_BIOMASS_Ecoli_core_w_GAM 	 0.321726
R_CYTBD 	 60.1398
B_R_ENO 	 8.10365
B_R_FBA 	 4.06453
R_FBP 	 3.97153
F_R_FUM 	 39.2006
B_R_GAPD 	 7.16438
F_R_GLUDy 	 5
R_GLUSy 	 1.17889
R_GND 	 0.228
B_R_ICDHyr 	 5.14604
R_ICL 	 1.82604
F_R_MDH 	 6.02322
R_ME2 	 5.28835
F_R_PGI 	 0.439046
F_R_PGK 	 7.16438
F_R_PGM 	 7.64569
R_PPCK 	 4.62969
R_PPS 	 4.69156
R_PYK 	 1.2356
B_R_RPE 	 1.18313
B_R_RPI 	 1.41113
R_SUCDi 	 65.1398
B_R_TALA 	 2.18931
F_R_TKT1 	 1.12232
B_R_TKT2 	 2.30545
B_R_TPI 	 4.06453
