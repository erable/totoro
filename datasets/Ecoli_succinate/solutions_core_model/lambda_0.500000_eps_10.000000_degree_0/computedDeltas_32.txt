M_glc__D_e	0
M_gln__L_c	-4.67504
M_gln__L_e	0
M_glu__L_c	-4.232
M_glu__L_e	0
M_glx_c	0
M_h2o_c	10
M_h2o_e	0
M_h_c	4.52891
M_h_e	0
M_icit_c	10
M_lac__D_c	10
M_lac__D_e	0
M_mal__L_c	27.889
M_mal__L_e	0
M_nad_c	0
M_13dpg_c	0
M_nadh_c	0
M_nadp_c	0
M_nadph_c	0
M_nh4_c	10
M_nh4_e	0
M_2pg_c	2.92685
M_o2_c	-39.6528
M_3pg_c	0
M_o2_e	0
M_oaa_c	-1.33227e-15
M_6pgc_c	-0.252
M_pep_c	-0.185
M_6pgl_c	0
M_pi_c	3.70362
M_ac_c	0
M_pi_e	0
M_pyr_c	0.921
M_pyr_e	0
M_q8_c	-2.65877
M_ac_e	0
M_q8h2_c	2.65877
M_acald_c	0
M_r5p_c	-0.100572
M_acald_e	0
M_ru5p__D_c	0
M_accoa_c	0
M_s7p_c	0
M_succ_c	-81.9643
M_acon_C_c	-3.32
M_succ_e	0
M_actp_c	0
M_succoa_c	0
M_adp_c	-0.269
M_xu5p__D_c	0
M_akg_c	-1.80171
M_akg_e	0
M_amp_c	4.06562
M_atp_c	-3.79662
M_cit_c	3.32
M_co2_c	9.56159
M_co2_e	0
M_coa_c	0
M_dhap_c	0
M_e4p_c	0
M_etoh_c	0
M_etoh_e	0
M_f6p_c	-0.073
M_fdp_c	0.093
M_for_c	2.46203
M_for_e	0
M_fru_e	0
M_fum_c	33.592
M_fum_e	0
M_g3p_c	-0.321894
M_g6p_c	-0.438397
M_biomass_c	3.28463
