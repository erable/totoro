############################################################################
#
#	Totoro 
#
###########################################################################
# Totoro is distributed under the CecILL Licence (http://www.cecill.info/);
# Ricardo De Andrade (ricardoluizandrade@gmail.com)
# Alice Julien-Laferrière (alice.julien.laferriere@gmail.com)
# Irene Ziska (irene.ziska@inria.fr)


Project Dependencies
====================
This project depends on the following external libraries:

    * libxml++-2.6 (Ubuntu package: libxml2-dev)
    \___ Depends on:
                 × glibmm-2.4 (Ubuntu package: libgmm++-dev)
                 × glib-2.0 (Ubuntu packages: libglib2.0-dev libglib2.0-0)
    * CPLEX/Concert-12.71
    * Boost.Program_options-1.0 (Ubuntu packages: libboost-program-options-dev)

This project also requires:

    cmake version 3.10.0

    GNU Make >= 4.1 and GNU Compiler for C/C++ >= 4.8
    (Ubuntu package: build-essential)

Compiling
==========
Install the required libraries.

Export your CPLEX path:

    export CPLEX_DIR=path_to_cplex/CPLEX_Studio1271

Go into the directory "totoro/" and do:

    mkdir build
    cd build
    cmake ..
    make

cmake assumes the standard install locations:

    /usr/include/libxml2
    /usr/include/libxml++-2.6
    /usr/lib/x86_64-linux-gnu/libxml++-2.6/include
    /usr/include/glibmm-2.4
    /usr/lib/x86_64-linux-gnu/glibmm-2.4/include
    /usr/include/glib-2.0
    /usr/lib/x86_64-linux-gnu/glib-2.0/include

They can be changed in the file src/CMakeLists.txt if necessary.
It might be necessary to include CPLEX in the LD_LIBRARY_PATH

Running
=======
To run Totoro, the provided "totoro/run/runStories_toModify.sh" can be used. The paths must be modified.
Inside the file, the following parameters can be set:

NETWORK    the path to the network file (xml file)
DELTAS     the path to the file that contains the deltas, tab seperated (min and max must either both be positive or both be negative for one metabolite)
OUTPUT     the path where the solutions are stored (directory)
OUTPUT2    the path to a solution file
EX         the path to a file that names all exchange reactions that should be blocked
MEM        maximum amount of memory that can be used
CPU        number of CPU
TIME       maximum amount of time that CPLEX can use to solve one iteration
ENUM       number of solutions that should be enumerated
PARAMETERS path to a file where parameters for CPLEX are set 
EPS        value for epsilon 
LAMBDA     value for lambda

The parameters for CPLEX can be set in the run/parameters.txt file. They must be modified to set the correct path for a working directory for CPLEX.

Output
======
- computed_Phis_i.txt, computed_Deltas_i.txt : rates and deltas for the solution i.
- isOptimal.txt : shows if the solver stopped because the time limit was reached or because the optimum was found
- reaction_occurences.txt : shows how often a reaction appeared in all solutions
- metabolites_overview : shows the chosen changes in the metabolite concentrations chosen by the solver for each iteration

