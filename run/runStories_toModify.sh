#!/bin/bash
PATH=PATH_TO_PROJECT_TO_MODIFY/totoro
NETWORK=$PATH/datasets/e_coli_core.xml
DELTAS=$PATH/datasets/Ecoli_pyruvate/deltas_pyruvate_pulse_pss_core_model.txt
OUTPUT=$PATH/datasets/Ecoli_pyruvate/solutions/
OUTPUT2=$PATH/datasets/Ecoli_pyruvate/solutions/solution.txt
EX=$PATH/datasets/exchange_reactions_core_model.txt
MEM=4000
CPU=1
TIME=100000
ENUM=10
PARAMETERS=$PATH/run/parameters_core_model.txt
EPS=5
LAMBDA=0.1

$PATH/build/Hyperstories -N $NETWORK -D $DELTAS --create-intermediate-output-files -c $CPU -m $MEM -t $TIME --max-numeration-iterations $ENUM --intermediates-output-files-folder $OUTPUT -o $OUTPUT2 -p $PARAMETERS --exchange-reactions-file $EX -e $EPS -l $LAMBDA --degree-constraint 0

