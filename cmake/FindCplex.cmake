#
# - Try to find Cplex
#
# Once done this will define
#
#   CPLEX_FOUND - system has Cplex
#   CPLEX_INCLUDE_DIRS - the Cplex (and deps) include directories
#   CPLEX_LIBRARIES - Link these to use Cplex
#
find_path(CPLEX_INCLUDE_DIR NAMES ilcplex
        PATHS $ENV{CPLEX_DIR}/cplex/include)
find_path(CPLEX_CONCERT_DIR NAMES ilconcert
        PATHS $ENV{CPLEX_DIR}/concert/include)

if (NOT CPLEX_INCLUDE_DIR)
    message(WARNING "\
    \nCould not find CPLEX \
    \nIf you want to use CPLEX, set the environment variable CPLEX_DIR to the \
    path of your CPLEX installation, e.g. \
    export CPLEX_DIR=path_to_cplex\n\
    ")
endif (NOT CPLEX_INCLUDE_DIR)

find_library(CPLEX_LIBRARY NAMES cplex
        PATHS $ENV{CPLEX_DIR}/cplex/lib/x86-64_osx/static_pic
        $ENV{CPLEX_DIR}/cplex/lib/x86-64_sles10_4.1/static_pic
        $ENV{CPLEX_DIR}/cplex/lib/x86-64_linux/static_pic)
find_library(CPLEX_CONCERT_LIBRARY NAMES concert
        PATHS $ENV{CPLEX_DIR}/concert/lib/x86-64_osx/static_pic
        $ENV{CPLEX_DIR}/concert/lib/x86-64_sles10_4.1/static_pic
        $ENV{CPLEX_DIR}/concert/lib/x86-64_linux/static_pic)
find_library(CPLEX_ILOCPLEX_LIBRARY NAMES ilocplex
        PATHS $ENV{CPLEX_DIR}/cplex/lib/x86-64_osx/static_pic
        $ENV{CPLEX_DIR}/cplex/lib/x86-64_sles10_4.1/static_pic
        $ENV{CPLEX_DIR}/cplex/lib/x86-64_linux/static_pic)

set(CPLEX_INCLUDE_DIRS ${CPLEX_INCLUDE_DIR})
set(CPLEX_CONCERT_INCLUDE_DIRS ${CPLEX_CONCERT_DIR})

get_filename_component(CPLEX_LIBRARY_PATH ${CPLEX_LIBRARY} DIRECTORY)
set(CPLEX_LIBRARIES ${CPLEX_LIBRARY_PATH})
get_filename_component(CPLEX_CONCERT_LIBRARY_PATH ${CPLEX_CONCERT_LIBRARY} DIRECTORY)
set(CPLEX_CONCERT_LIBRARIES ${CPLEX_CONCERT_LIBRARY_PATH})
get_filename_component(CPLEX_ILOCPLEX_LIBRARY_PATH ${CPLEX_ILOCPLEX_LIBRARY} DIRECTORY)
set(CPLEX_ILOCPLEX_LIBRARIES ${CPLEX_ILOCPLEX_LIBRARY_PATH})

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Cplex DEFAULT_MSG
        CPLEX_INCLUDE_DIRS CPLEX_CONCERT_INCLUDE_DIRS CPLEX_LIBRARIES CPLEX_CONCERT_LIBRARIES CPLEX_ILOCPLEX_LIBRARIES)
