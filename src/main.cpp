/* ***************************************************************************
 *
 *                              Totoro
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Ricardo Andrade
 *                  Irene Ziska
 *                  Alice Julien-Laferriere
 *                  Laurent Bulteau
 *                  Arnaud Mary
 *
 * ricardoluizandrade@gmail.com
 * irene.ziska@inria.fr
 * alice.julien.laferriere@gmail.com
 **
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include <iostream>
#include <list>
#include <cstring>
#include <string>
#include <unistd.h>
#include <sys/stat.h>

#include "hypergraph/Decomposition.h"
#include "cplex_interface/cplex_modeler.h"
#include "io/read_writeFile.h"
#include "io/write_output.h"
#include "io/InputParser.h"

// arg 1 : xml file
// arg2  : bn file
int main(int argc, char** argv)
{
    char directory[1024];
    if (getcwd(directory, sizeof(directory)) != NULL)
        fprintf(stdout, "Current working dir: %s\n", directory);
    else
    {
        perror("getcwd() error");
        return -9;
    }

    CmdLineParser parser;
    try
    {
        parser.parseInput(argc, argv);
    }
    catch (const std::invalid_argument& e)
    {
        cout << e.what() << endl;
        return -3;
    }
    catch (const std::exception& e)
    {
        cout << e.what() << endl;
        return 0;
    }

    if (parser.didAskedForHelp())
    {
        parser.printHelpDescription();
        return 0;
    }
    HGraph graph;
    if (parser.getNetworkFileName().size() > 0)
    {
        cout << "Reading " << parser.getNetworkFileName() << endl;
        readXML(parser.getNetworkFileName().c_str(), graph);
    }
    else
    {
        cerr << "No input SBML file." << endl;
        parser.printHelpDescription();
        return -1;
    }

    if (parser.getDeltaFileName().size() > 0)
    {
        cout << "Reading " << parser.getDeltaFileName() << endl;
        readBNfile(parser.getDeltaFileName().c_str(), graph);
    }
    else
    {
        cerr << "No delta/black node file" << endl;
        parser.printHelpDescription();
        return -2;
    }

    if (parser.shouldFilterCofactors())
    {
        cout << "Reading cofactors from file: " << parser.getCofactorsIdListFileName() << endl;
        filterCofactorsFromFile(parser.getCofactorsIdListFileName().c_str(), graph);
    }

    std::vector<int> idsToRemove;
    for (const auto& mapEntry : graph.getNodes())
    {
        if (mapEntry.second.getInDegree() == 0 && mapEntry.second.getOutDegree() == 0)
        {
            idsToRemove.emplace_back(mapEntry.first);
        }
    }
    for (const auto& id : idsToRemove)
    {
        graph.removeNode(id);
    }

    // Re-index the graph so all ids will begin in 0 and be sequential.
    graph.reIndex();

    // Write a xml with the modified network that will be used in the method to be used with dinghy
    // TODO: Include this as an command line option.
//    writeXML("../test.xml", graph);

    if (parser.shouldReadBoundsFile())
    {
        readReactionBoundsFile(parser.getBoundsFilename().c_str(), graph);
    }

    double lambda = parser.getLambda();
    double epsilon = parser.getDeltaEpsilon();
    float degree = parser.getDegreeConstraintBound();

    mkdir(parser.getFolderIntermediateFiles().c_str(), 0777);

    string outputFolder =
            parser.getFolderIntermediateFiles() + "lambda_" + to_string(lambda) + "_eps_" + to_string(epsilon)
                    + "_degree_" + to_string(degree) + "/";

    cplex_modeler solver(graph, lambda, epsilon, parser.getOptimizerTimeout(), parser.getMaxEnumerationIterations(),
            parser.getOptimizerInternalMemory(), parser.getOptimizerCPUAmount());

    if (parser.shouldCreateIntermediateFiles())
    {
        solver.setCreateIntermediaryFiles(true);
        solver.setIntermediaryFilesFolder(outputFolder);
        mkdir(outputFolder.c_str(), 0777);
        cout << "Intermediate output files will be generated in folder: " << outputFolder << endl;
    }
    if (parser.shouldTuneSolver())
    {
        solver.setTuneSolver(true);
        solver.setParametersFileName(parser.getSolverParametersFileName());
        cout << "Tuning solver and saving the parameters in the file: " << parser.getSolverParametersFileName()
             << endl;
    }
    if (parser.shouldParseExchangeReactions())
    {
        solver.setExchangeReactionsFile(parser.getExchangeReactionsFilename());
    }
    if (parser.shouldReadSolverParametersFile())
    {
        solver.setReadParametersFile(true);
        solver.setParametersFileName(parser.getSolverParametersFileName());
        cout << "Reading solver parameters from file: " << parser.getSolverParametersFileName() << endl;
    }
    if (parser.shouldFixAmountOfUsedReactions())
    {
        int amount = parser.getAmountOfUsedReactions();
        solver.setMinimumAmountOfUsedReactions(amount);
    }
    if (parser.shouldFixAbsVariationOfNonMeasuredCompounds())
    {
        double variation = parser.getAbsVariationOfNonMeasuredCompounds();
        solver.setMinimumAbsoluteVariationOfNonMeasuredCompounds(variation);
    }
    if (parser.getDegreeConstraintBound() > 0)
    {
        solver.setMaximumDegreeConstraintBound(degree);
    }

    vector<Solution> solutions;
    if (parser.getReactionIdsToMaximise().size() > 0 || parser.getReactionIdsToMaximise().size() < 0)
    {
        cout << "----------- Alternative mode : FBA -----------" << endl;
        vector<HEdge> reactions;
        cout << "Maximise reactions : ";
        for (string ename : parser.getReactionIdsToMaximise())
        {
            boost::optional<HEdge> e = graph.getEdge(ename);
            if (e.is_initialized())
            {
                reactions.push_back(*e);
                cout << ename << ' ';
            }
            else
            {
                cerr << "\n Could not find reaction " << ename << endl;
            }
        }
        cout << endl;
        solutions = solver.doFBAMinimizeAmountOfUsedReactions(reactions, maximise);
        set<HEdge> anthologySolution;
        if (solutions.size() > 0)
        {
            cout << "Solutions found:" << endl;
            cout << "----------------" << endl;
            int nreactions = 0;
            for (Solution s : solutions)
            {
                for (HEdge e : s.getReactionsInSolution())
                {
                    anthologySolution.insert(e);
                }
            }
            for (HEdge e : anthologySolution)
            {
                cout << e.getName() << endl;
                ++nreactions;
            }
            cout << "----------------" << endl;
            cout << "Total of " << nreactions << " reactions" << endl;
        }
    }
    else
    {
        solutions = solver.solveModel();
        if (solutions.size() > 0)
        {
            // Display & writing Sol
            cout << "Solutions found:" << endl;
            cout << "----------------" << endl;
            int solutions_counter = 0;
            for (Solution s : solutions)
            {
                ++solutions_counter;
                cout << "Solution " << solutions_counter << ":" << endl;
                s.display();
            }
            string outfile = outputFolder + "solutions.txt";
            writeOutput(solutions, outfile);
            cout << "Solutions wrote to the file " << outfile << " ." << endl;
        }
        else
        {
            cout << "Found no solutions." << endl;
        }
    }

    return 0;
}
