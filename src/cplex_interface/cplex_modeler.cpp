/* ***************************************************************************
 *
 *                              Totoro
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Ricardo Andrade
 *                  Irene Ziska
 *                  Alice Julien-Laferriere
 *                  Laurent Bulteau
 *                  Arnaud Mary
 *
 * ricardoluizandrade@gmail.com
 * irene.ziska@inria.fr
 * alice.julien.laferriere@gmail.com
 **
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "cplex_modeler.h"

cplex_modeler::cplex_modeler(const HGraph& graph, const double lambda, const double epsilon, const int timeout,
        const unsigned int maxIterations, const int ramMB, const int cpus)
        :_env(IloEnv()), _model(_env), cplex(_model), objective(_env), _graph(graph), _lambda(lambda),
         _epsilon(epsilon), _timeout(timeout), _maxIterations(maxIterations), _ramMB(ramMB), _cpus(cpus)
{
    assert((_lambda >= 0) && (_lambda <= 1.0));
}

//Model Declaration
vector<Solution> cplex_modeler::solveModel()
{
    bool isAmountOfReactionsInSolutionFixed = false;
    bool isAbsVarNonMeasuredFixed = false;

    vector<Solution> solutions;

    // Number of lines and columns
    const int ncolsS = _graph.getNbEdges();
    const int nlinesS = _graph.getNbNodes();
    cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

    computeMaxAbsoluteMeasuredVariation();

    IloModel hyperstory(_env);
    hyperstory.setName("HyperstoryDefaultModel");

    createPhisVars();
    createDeltaVars();
    createPhiConstraints();
    createBinaries();
    createBinaryConstraints();

    createMinNumberUsedReactions();

    if (_lambda != 1)
    {
        createAbsDeltaVars();
        createAbsDeltaConstraints();
    }

    if (_maxDegreeConstraintBound > 0)
    {
        createMaxDegreeConstraints();
    }

    createObjectiveFunction();

    readParameters();
    tuneSolver();

    ofstream osOptimalValues;
    osOptimalValues.open(_intermediary_files_folder + "optimalValues.txt", std::ios::trunc);

    std::ofstream osIfNotOptimal;
    osIfNotOptimal.open(_intermediary_files_folder + "isOptimal.txt", std::ios::trunc);

    bool hasMoreSolutions = true;
    while (hasMoreSolutions)
    {
        cplex.exportModel((_intermediary_files_folder + "/stories.lp").c_str());
        cplex.solve();
        cout << "[###] Solver status: " << cplex.getStatus() << endl;

        if (cplex.getStatus() == Optimal || cplex.getStatus() == 1)
        {
            if (cplex.getStatus() == Optimal)
            {
                osIfNotOptimal << "optimal\n";
            }
            else
            {
                osIfNotOptimal << "not optimal\n";
            }
            osIfNotOptimal.flush();

            cout << "[###]  Optimum is :  " << cplex.getObjValue() << endl;
            osOptimalValues << cplex.getObjValue() << "\n";
            osOptimalValues.flush();
            Solution solution = getSolutionValues();

            if (solution.size() > 0)
            {
                solutions.emplace_back(solution);
                if (this->_create_intermediary_files)
                {
                    int idSolo = solutions.size();
                    solution.writePhis_sol(idSolo, this->_intermediary_files_folder.c_str(), "computed");

                    std::ofstream os;
                    os.open(_intermediary_files_folder + "reactions_" + std::to_string(idSolo) + ".txt",
                            std::ios::trunc);

                    os << "Reactions\tColorReaction\n";
                    for (const auto& reaction : solution.getReactionsInSolution())
                    {
                        std::string name = reaction.getName();
                        if (name.substr(0, 2) == "B_")
                        {
                            os << name.substr(2, name.length() - 2) << "\t-10\n";
                        }
                        else if (name.substr(0, 2) == "F_")
                        {
                            os << name.substr(2, name.length() - 2) << "\t10\n";
                        }
                        else
                        {
                            os << name << "\t10\n";
                        }
                    }
                    os.close();
                }
            }

            cout << "[###] Solution with " << solution.getReactionsInSolution().size() << " reactions." << endl;
            excludeCurrentSolution(solution, solutions.size());
        }
        else
        {
            hasMoreSolutions = false;
        }
        if ((_maxIterations > 0) && (solutions.size() >= _maxIterations))
        {
            cerr << "Reaching maximum of enumeration iterations (" << _maxIterations << "), stopping." << endl;
            hasMoreSolutions = false;
        }
    }

    osIfNotOptimal.close();
    osOptimalValues.close();

    cplex.clear();
    cplex.end();

    std::unordered_map<std::string, int> occurences;

    for (const auto& solution : solutions)
    {
        for (const auto& edge : solution.getReactionsInSolution())
        {
            if (occurences.find(edge.getName()) != occurences.end())
            {
                occurences.at(edge.getName())++;
            }
            else
            {
                occurences.emplace(edge.getName(), 1);
            }
        }
    }

    std::ofstream printOc;
    printOc.open(_intermediary_files_folder + "reaction_occurences.txt", std::ios::trunc);
    std::ofstream printOcJson;
    printOcJson.open(_intermediary_files_folder + "reaction_occurences.json", std::ios::trunc);

    printOcJson << "{ ";

    bool first = true;

    for (const auto& entry : occurences)
    {
        printOc << entry.first << "\t" << entry.second << "\n";

        int pos = entry.first.find("R_");
        std::string substring = entry.first.substr(pos + 2, entry.first.length() - pos + 2);

        if (first)
        {
            printOcJson << "\"" << substring << "\" : " << entry.second;
            first = false;
        }
        else
        {
            printOcJson << ", \"" << substring << "\" : " << entry.second;
        }

    }

    printOcJson << " }";
    printOcJson.close();
    printOc.close();

    std::ofstream osMetabolites;
    osMetabolites.open(_intermediary_files_folder + "metabolites_overview.txt", std::ios::trunc);
    for (int i = 0; i < _graph.getNbNodes(); ++i)
    {
        osMetabolites << solutions.at(0).getDeltasComputed().at(i).first << "\t";

        for (const auto& solution : solutions)
        {
            osMetabolites << solution.getDeltasComputed().at(i).second << "\t";
        }

        osMetabolites << "\n";
    }

    osMetabolites.close();

    return solutions;
}

void cplex_modeler::computeMaxAbsoluteMeasuredVariation()
{
    maxAbsMeasuredVariation = 0.0;

    for (const int id : _graph.getBlackNodes())
    {
        double minDelta = _graph.getNode(id).getLBConcentrationValue();
        double maxDelta = _graph.getNode(id).getUBConcentrationValue();
        if (abs(minDelta) > maxAbsMeasuredVariation)
        {
            maxAbsMeasuredVariation = abs(minDelta);
        }
        if (abs(maxDelta) > maxAbsMeasuredVariation)
        {
            maxAbsMeasuredVariation = abs(maxDelta);
        }
    }
}

void cplex_modeler::createPhisVars()
{
    double defaultLowerBound = 0.0;
    double defaultUpperBound = 100 * maxAbsMeasuredVariation;
//    std::string exchange = "R_EX_";

    for (const auto& reaction : _graph.getEdges())
    {
        std::string varName = "p_" + reaction.second.getName();

        bool isExchange = false;

        for (const auto& exchangeReaction : exchangeReactions)
        {
            if (reaction.second.getName().find(exchangeReaction) != std::string::npos)
            {
                isExchange = true;
                break;
            }
        }

//        if (reaction.second.getName().find(exchange) != std::string::npos)
        if (isExchange)
        {
            phis.emplace(varName, IloNumVar(_env, 0.0, 0.0, ILOFLOAT, varName.c_str()));
        }
        else
        {
            phis.emplace(varName, IloNumVar(_env, defaultLowerBound, defaultUpperBound, ILOFLOAT, varName.c_str()));
        }
        _model.add(phis.at(varName));
    }
}

void cplex_modeler::createDeltaVars()
{
    for (const auto& metabolite : _graph.getNodes())
    {
        std::string varName = "delta_" + metabolite.second.getName();

        std::string extracellular = "_e";

        if (metabolite.second.isBlack())
        {
            deltas.emplace(varName, IloNumVar(_env, metabolite.second.getLBConcentrationValue(),
                    metabolite.second.getUBConcentrationValue(), ILOFLOAT, varName.c_str()));
        }
        else if (metabolite.second.getName().find(extracellular) != std::string::npos)
        {
            deltas.emplace(varName, IloNumVar(_env, 0.0, 0.0, ILOFLOAT, varName.c_str()));
        }
        else
        {
            deltas.emplace(varName, IloNumVar(_env, -_epsilon, _epsilon, ILOFLOAT, varName.c_str()));
        }
        _model.add(deltas.at(varName));
    }
}

void cplex_modeler::createPhiConstraints()
{
    for (const auto& metabolite : _graph.getNodes())
    {
        std::string constraintName = "Sb_" + metabolite.second.getName();

        phiConstraints.emplace(constraintName, IloRange(_env, 0.0, 0.0, constraintName.c_str()));

        IloRange* cons = &phiConstraints.at(constraintName);

        cons->setLinearCoef(deltas.at("delta_" + metabolite.second.getName()), -1.0);

        for (const int id : metabolite.second.getInEdges())
        {
            auto reaction = _graph.getEdge(id);

            auto itHeadStoichio = reaction.getHeadStoichiometry().begin();
            for (auto itHeadNodes = reaction.getHeadNodes().begin();
                 itHeadNodes != reaction.getHeadNodes().end();
                 ++itHeadNodes, ++itHeadStoichio)
            {
                if (*itHeadNodes == metabolite.first)
                {
                    cons->setLinearCoef(phis.at("p_" + reaction.getName()), *itHeadStoichio);
                    break;
                }
            }

        }

        for (const int id : metabolite.second.getOutEdges())
        {
            auto reaction = _graph.getEdge(id);

            auto itTailStoichio = reaction.getTailStoichiometry().begin();
            for (auto itTailNodes = reaction.getTailNodes().begin();
                 itTailNodes != reaction.getTailNodes().end();
                 ++itTailNodes, ++itTailStoichio)
            {
                if (*itTailNodes == metabolite.first)
                {
                    cons->setLinearCoef(phis.at("p_" + reaction.getName()), -(*itTailStoichio));
                    break;
                }
            }
        }

        _model.add(*cons);
    }
}

void cplex_modeler::createBinaries()
{
//    std::string exchange = "R_EX";
    for (const auto& reaction : _graph.getEdges())
    {
        std::string varName = "u_" + reaction.second.getName();

        bool isExchange = false;
        for (const auto& exchangeReaction : exchangeReactions)
        {
            if (reaction.second.getName().find(exchangeReaction) != std::string::npos)
            {
                isExchange = true;
                break;
            }
        }

//        if (reaction.second.getName().find(exchange) != std::string::npos)
        if (isExchange)
        {
            binsIsUsed.emplace(varName, IloBoolVar(_env, 0, 0, varName.c_str()));
        }
        else
        {
            binsIsUsed.emplace(varName, IloBoolVar(_env, 0, 1, varName.c_str()));
        }

        _model.add(binsIsUsed.at(varName));
    }
}

void cplex_modeler::createBinaryConstraints()
{
    for (const auto& reaction : _graph.getEdges())
    {
        std::string constraintName = "using_" + reaction.second.getName();
        IloNumVar* phi = &phis.at("p_" + reaction.second.getName());
        IloBoolVar* binary = &binsIsUsed.at("u_" + reaction.second.getName());
        binaryConstraints.emplace(constraintName, IloRange(_env, -IloInfinity, 0.0, constraintName.c_str()));
        IloRange* cons = &binaryConstraints.at(constraintName);
        cons->setLinearCoef(*phi, 1.0);
        cons->setLinearCoef(*binary, -phi->getUB());
//        binaryConstraints.emplace(constraintName, IloIfThen(_env, *phi != 0, *binary >= 0.5, constraintName.c_str()));
        _model.add(binaryConstraints.at(constraintName));
    }
}

void cplex_modeler::createMinNumberUsedReactions()
{
    std::string constraintName = "MinNumberUsedReactions";
    minNumberUsedReaction = IloRange(_env, _min_used_reactions, IloInfinity, constraintName.c_str());

    for (const auto& entry : binsIsUsed)
    {
        minNumberUsedReaction.setLinearCoef(entry.second, 1.0);
    }

    _model.add(minNumberUsedReaction);
}

void cplex_modeler::createAbsDeltaVars()
{
    for (const auto& metabolite : _graph.getNodes())
    {
        if (metabolite.second.isBlack())
        {
            continue;
        }

        std::string varName = "absDelta_" + metabolite.second.getName();

        absDeltas.emplace(varName, IloNumVar(_env, 0, IloInfinity, varName.c_str()));
        _model.add(absDeltas.at(varName));
    }
}

void cplex_modeler::createAbsDeltaConstraints()
{
    for (const auto& metabolite : _graph.getNodes())
    {
        if (metabolite.second.isBlack())
        {
            continue;
        }

        std::string constraintName1 = "absDeltaCons1_" + metabolite.second.getName();
        std::string constraintName2 = "absDeltaCons2_" + metabolite.second.getName();

        IloNumVar* absDelta = &absDeltas.at("absDelta_" + metabolite.second.getName());
        IloNumVar* delta = &deltas.at("delta_" + metabolite.second.getName());

        absDeltaConstraints.emplace(constraintName1, IloRange(_env, -IloInfinity, 0, constraintName1.c_str()));
        IloRange* cons1 = &absDeltaConstraints.at(constraintName1);
        cons1->setLinearCoef(*absDelta, -1.0);
        cons1->setLinearCoef(*delta, 1.0);
        _model.add(*cons1);

        absDeltaConstraints.emplace(constraintName2, IloRange(_env, -IloInfinity, 0, constraintName2.c_str()));
        IloRange* cons2 = &absDeltaConstraints.at(constraintName2);
        cons2->setLinearCoef(*absDelta, -1.0);
        cons2->setLinearCoef(*delta, -1.0);
        _model.add(*cons2);
    }
}

void cplex_modeler::createMaxDegreeConstraints()
{
    for (const auto& metabolite : _graph.getNodes())
    {
        std::string constraintName = "maxDegree_" + metabolite.second.getName();
        maxDegreeConstraints
                .emplace(constraintName, IloRange(_env, 0, _maxDegreeConstraintBound, constraintName.c_str()));
        IloRange* cons = &maxDegreeConstraints.at(constraintName);

        for (const int edgeId : metabolite.second.getInEdges())
        {
            cons->setLinearCoef(binsIsUsed.at("u_" + _graph.getEdge(edgeId).getName()), 1.0);
        }

        for (const int edgeId : metabolite.second.getOutEdges())
        {
            cons->setLinearCoef(binsIsUsed.at("u_" + _graph.getEdge(edgeId).getName()), 1.0);
        }
        _model.add(*cons);
    }
}

void cplex_modeler::createObjectiveFunction()
{
    objective.setSense(IloObjective::Sense::Minimize);

//    std::string exchange = "R_EX_";
    for (const auto& entry : binsIsUsed)
    {
        bool isExchange = false;
        for (const auto& exchangeReaction : exchangeReactions)
        {
            if (entry.first.find(exchangeReaction) != std::string::npos)
            {
                isExchange = true;
                break;
            }
        }
//        if (entry.first.find(exchange) != std::string::npos)
        if (isExchange)
        {
            objective.setLinearCoef(entry.second, 100 * _lambda);
        }
        else
        {
            objective.setLinearCoef(entry.second, _lambda);
        }
    }

    if (_lambda != 1)
    {
        for (const auto& entry : absDeltas)
        {
            objective.setLinearCoef(entry.second, 1.0 - _lambda);
        }

        for (const auto& id : _graph.getBlackNodes())
        {
            auto metabolite = _graph.getNode(id);

            IloNumVar* delta = &deltas.at("delta_" + metabolite.getName());

            if (metabolite.getLBConcentrationValue() < 0 && metabolite.getUBConcentrationValue() <= 0)
            {
                objective.setLinearCoef(*delta, 1.5 * (1.0 - _lambda));
            }
            else if (metabolite.getLBConcentrationValue() >= 0 && metabolite.getUBConcentrationValue() > 0)
            {
                objective.setLinearCoef(*delta, -1.5 * (1.0 - _lambda));
            }
            else
            {
                std::cerr << metabolite.getName() << " - Wrong bounds for black node! \n";
            }

        }
    }
    _model.add(objective);
}

void cplex_modeler::readParameters()
{
    if (this->_should_read_parameters)
    {
        cout << "Reading the internal solver parameters from: " << _parameters_file << endl;
        cplex.readParam(_parameters_file.c_str());
    }
    cplex.setParam(IloCplex::IntParam::NodeFileInd, 3);
    cplex.setParam(IloCplex::Param::Threads, _cpus);
    cplex.setParam(IloCplex::Param::WorkMem, _ramMB);
    cout << "Setting time-out to " << this->_timeout << endl;
    cplex.setParam(IloCplex::Param::TimeLimit, this->_timeout);
    cplex.setParam(IloCplex::Param::MIP::Tolerances::Linearization, 1e-6);
}

void cplex_modeler::tuneSolver()
{
    if (this->_shoud_tune_solver)
    {
        cout << "Tuning the internal solver and writing the parameters to: " << _parameters_file << endl;
        IloInt tuneStat = cplex.tuneParam();
        cplex.writeParam(_parameters_file.c_str());
        if (tuneStat == IloCplex::TuningComplete)
            cout << "Tuning complete." << endl;
        else if (tuneStat == IloCplex::TuningAbort)
            cout << "Tuning abort." << endl;
        else if (tuneStat == IloCplex::TuningTimeLim)
            cout << "Tuning time limit." << endl;
        else
            cout << "Tuning status unknown." << endl;
    }
}

Solution cplex_modeler::getSolutionValues()
{
    Solution solution;

    for (const auto& reaction : _graph.getEdges())
    {
        auto binVar = binsIsUsed.at("u_" + reaction.second.getName());
        int binaryValue = (int) (cplex.getValue(binVar) + 0.5);

        if (binaryValue == 1)
        {
            auto phiVar = phis.at("p_" + reaction.second.getName());
            double phiValue = cplex.getValue(phiVar);
            solution.addEdgeToSolution(reaction.second, phiValue);
        }
    }

    for (const auto& metabolite : _graph.getNodes())
    {
        auto deltaVar = deltas.at("delta_" + metabolite.second.getName());
        double deltaValue = cplex.getValue(deltaVar);
        solution.addNodeToSolution(metabolite.second.getName(), deltaValue);
    }

    return solution;
}

void cplex_modeler::excludeCurrentSolution(const Solution& solution, int counter)
{
    std::string constraintName = "ExcludeSolution_" + std::to_string(counter);

    excludeSolutionConstraints.emplace(constraintName,
            IloRange(_env, 0, solution.getReactionsInSolution().size() - 1, constraintName.c_str()));

    IloRange* cons = &excludeSolutionConstraints.at(constraintName);

    for (const auto& usedReaction : solution.getReactionsInSolution())
    {
        cons->setLinearCoef(binsIsUsed.at("u_" + usedReaction.getName()), 1.0);
    }

    _model.add(*cons);
}

vector<Solution>
cplex_modeler::doFBAMinimizeAmountOfUsedReactions(const vector<HEdge> reactionsToProduce, const FBAMode mode)
{
    // Number of lines and columns
    const unsigned int ncolsS = _graph.getNbEdges();
    const unsigned int nlinesS = _graph.getNbNodes();
    cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

    IloNumArray inputDeltas(_env, nlinesS);
    for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
    {
        int id = nodeit->first;
        inputDeltas[id] = 0.0;
    }
    for (const int id : _graph.getBlackNodes())
    {
        inputDeltas[id] = 0.0;
    }
    IloModel hyperstory(_env);
    IloNumVarArray fluxArray(_env, ncolsS);
    IloNumVarArray isUsed(_env, ncolsS);

    try
    {
        // Model variables
        // For each flux
        for (unsigned int i = 0; i < ncolsS; ++i)
        {
            std::string vname = this->_graph.getEdge(i).getName();
            fluxArray[i] = IloNumVar(_env, -IloInfinity, IloInfinity, ILOFLOAT, vname.c_str());
            std::string uname = "used_" + vname;
            isUsed[i] = IloNumVar(_env, 0, 1, ILOBOOL, uname.c_str());
        }

        // Constraints
        for (unsigned int i = 0; i < nlinesS; ++i)
        {
            bool addSSConstraint = false;
            IloNumExpr variationOfCompound(_env);
            for (auto edge_it = _graph.getEdges().begin(); edge_it != _graph.getEdges().end(); edge_it++)
            {
                int edge_id = edge_it->first;
                HEdge edge = edge_it->second;
                const list<int> sources = edge.getTailNodes();
                const list<double> sourcesStoich = edge.getTailStoichiometry();
                list<int>::const_iterator it1;
                list<double>::const_iterator it2;
                for (it1 = sources.begin(), it2 = sourcesStoich.begin();
                     (it1 != sources.end()) && (it2 != sourcesStoich.end());
                     ++it1, ++it2)
                {
                    int nodeId = *it1;
                    double nodeStoich = *it2;
                    if (nodeId == (int) i)
                    {
                        addSSConstraint = true;
                        variationOfCompound -= nodeStoich * fluxArray[edge_id];
                    }
                }
                list<int> products = edge.getHeadNodes();
                list<double> productsStoich = edge.getHeadStoichiometry();
                for (it1 = products.begin(), it2 = productsStoich.begin();
                     (it1 != products.end()) && (it2 != productsStoich.end());
                     ++it1, ++it2)
                {
                    int nodeId = *it1;
                    double nodeStoich = *it2;
                    if (nodeId == (int) i)
                    {
                        addSSConstraint = true;
                        variationOfCompound += nodeStoich * fluxArray[edge_id];
                    }
                }
            }
            // Sv[i] = b[i]
            if (addSSConstraint)
            {
                if (!this->_graph.getNode(i).isBoundary())
                {
                    std::string svbname = "(Sv=b)_" + this->_graph.getNode(i).getName() + "_" + std::to_string(i);
                    auto svconstraint = hyperstory.add(variationOfCompound == 0);
                    svconstraint.setName(svbname.c_str());
                }
            }
        }

        // Constraints for reactions
        for (unsigned int j = 0; j < ncolsS; ++j)
        {
            IloNumExpr lBound(_env);
            IloNumExpr uBound(_env);

            lBound += max(0.0, _graph.getEdge(j).getFluxLowerBound());
            uBound += min(100000.0, _graph.getEdge(j).getFluxUpperBound());
            std::string lbname = "flux_lo_bound_" + std::to_string(j);
            auto lbconstraint = hyperstory.add(lBound <= fluxArray[j]);
            lbconstraint.setName(lbname.c_str());
            std::string ubname = "flux_up_bound_" + std::to_string(j);
            auto ubconstraint = hyperstory.add(uBound >= fluxArray[j]);
            ubconstraint.setName(ubname.c_str());
        }

        for (unsigned int j = 0; j < ncolsS; ++j)
        {
            auto reactionIsUsedCut2 = hyperstory.add(isUsed[j] <= fluxArray[j] * 1e7);
            reactionIsUsedCut2.setName(("reactionIsUsedCut1" + _graph.getEdge(j).getName()).c_str());
            auto reactionIsUsedCut3 = hyperstory.add(fluxArray[j] <= isUsed[j] * 1e3);
            reactionIsUsedCut3.setName(("reactionIsUsedCut2" + _graph.getEdge(j).getName()).c_str());
            // If it is a reversible reaction add a constraint to forbid using both in a solution
            string reactionName = this->_graph.getEdge(j).getName();
            if (reactionName.compare(0, 1, "+") == 0)
            {
                for (unsigned int k = j + 1; k < ncolsS; ++k)
                {
                    string reactionRevName = this->_graph.getEdge(k).getName();
                    if (reactionName.substr(1).compare(reactionRevName.substr(1)) == 0)
                    {
                        auto forbidReactionAndReverse = hyperstory.add(isUsed[j] + isUsed[k] <= 1);
                        forbidReactionAndReverse
                                .setName(("forbidReactionAndReverse_" + _graph.getEdge(j).getName()).c_str());
                        break;
                    }

                }

            }
        }

        // Objective function
        IloNumExpr objective(_env);
        int obj_mult = 1;
        if (mode == minimise)
        {
            obj_mult = -1;
        }
        for (HEdge e : reactionsToProduce)
        {
            objective += fluxArray[e.getId()] * obj_mult;
        }
        for (pair<int, HEdge> e : _graph.getEdges())
        {
            if (e.second.getName().compare(1, 5, "R_EX_") == 0)
            {
                objective -= 0.1 * fluxArray[e.first];
            }
        }
        //hyperstory.add(objective >= 0.01);
        hyperstory.add(IloMaximize(_env, objective));
    }
    catch (IloException& exception)
    {
        const char* msg = exception.getMessage();
        cout << msg << endl;
    }

    // Return vector with all minimal solutions found
    vector<Solution> solutions;

    try
    {
        bool hasMoreSolutions = true;
        if (this->_should_read_parameters)
        {
            cout << "Reading the internal solver parameters from: " << _parameters_file << endl;
        }
        while (hasMoreSolutions)
        {
            IloCplex solver(_env);
            if (this->_should_read_parameters)
            {
                solver.readParam(_parameters_file.c_str());
            }
            solver.setOut(_env.getNullStream());
            solver.setWarning(_env.getNullStream());
            solver.setParam(IloCplex::Param::WorkDir, ".");
            solver.setParam(IloCplex::IntParam::NodeFileInd, 3);
            solver.setParam(IloCplex::Param::Threads, _cpus);
            solver.setParam(IloCplex::Param::WorkMem, _ramMB);
            solver.setParam(IloCplex::Param::TimeLimit, this->_timeout);
            solver.setParam(IloCplex::Param::MIP::Tolerances::MIPGap, 1e-7);
            solver.setParam(IloCplex::Param::MIP::Tolerances::Linearization, 1e-7);
            solver.setParam(IloCplex::Param::Tune::Display, 2);

            solver.extract(hyperstory);
            solver.exportModel("/tmp/fba.lp");
            solver.solve();

            //cout << "[###] Solver status: " << solver.getStatus() << " - Objective = " << solver.getObjValue() <<endl;

            if (solver.getStatus() == Optimal)
            {
                // Get the solution
                int amountOfZeroFluxes = 0;
                string reactionsToFixAtZero = "";
                Solution sol;

                IloNumArray fluxesSolTmp(_env, ncolsS);
                solver.getValues(fluxesSolTmp, fluxArray);
                std::vector<double> fluxesSol = std::vector<double>(ncolsS);
                for (unsigned int l = 0; l < ncolsS; ++l)
                {
                    fluxesSol[l] = fluxesSolTmp[l];
                    if (fluxesSol[l] <= 1e-7)
                    {
                        ++amountOfZeroFluxes;
                    }
                    else
                    {
                        HEdge e = _graph.getEdge(l);
                        sol.addEdgeToSolution(e, fluxesSol[l]);
                    }
                }
                solutions.push_back(sol);

                IloNumArray isUsedTmp(_env, ncolsS);
                solver.getValues(isUsedTmp, isUsed);
                std::vector<int> isUsedSol = std::vector<int>(ncolsS);

                IloNumExpr exclude_sol_expr(_env);
                int nToExclude = 0;
                for (unsigned int i = 0; i < isUsedSol.size(); ++i)
                {
                    isUsedSol[i] = ((int) (isUsedTmp[i] + 0.5));
                    if (isUsedSol[i] == 1)
                    {
                        exclude_sol_expr += isUsed[i];
                        nToExclude++;
                    }
                }
                if (nToExclude > 0)
                {
                    auto excludeCnst = hyperstory.add(exclude_sol_expr <= nToExclude - 1);
                    excludeCnst.setName("SolToExclude_");
                }
                cout << "." << flush;
            }
            else
            {
                hasMoreSolutions = false;
            }
            if ((_maxIterations > 0) && (solutions.size() > _maxIterations))
            {
                cerr << "Reaching maximum of enumeration iterations (" << _maxIterations << "), stopping." << endl;
                hasMoreSolutions = false;
            }
        }
    }
    catch (IloException& exception)
    {
        const char* msg = exception.getMessage();
        cout << msg << endl;
    }

    return solutions;
}



