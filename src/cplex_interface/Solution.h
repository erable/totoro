/* ***************************************************************************
 *
 *                              Totoro
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Ricardo Andrade
 *                  Irene Ziska
 *                  Alice Julien-Laferriere
 *                  Laurent Bulteau
 *                  Arnaud Mary
 *
 * ricardoluizandrade@gmail.com
 * irene.ziska@inria.fr
 * alice.julien.laferriere@gmail.com
 **
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#ifndef SRC_CPLEX_INTERFACE_SOLUTION_H_
#define SRC_CPLEX_INTERFACE_SOLUTION_H_

#include<vector>
#include <iostream>
#include <fstream>
#include"../hypergraph/HEdge.h"

class Solution {
public:
   Solution();
   virtual ~Solution();
   void addEdgeToSolution(HEdge edge, double phi);
   void addNodeToSolution(const string name, const double delta );
   const void writePhis_sol(const int id_sol, const char* folder, const char * filename);
   const string getBriefSolutionString();


   int size();
   void display();

   const std::vector<pair<string, double> >& getDeltasComputed() const {
      return deltasComputed;
   }

   void setDeltasComputed(
         const std::vector<pair<string, double> >& deltasComputed) {
      this->deltasComputed = deltasComputed;
   }

   const std::vector<double>& getPhisComputed() const
   {
      return phisComputed;
   }

   void setPhisComputed(const std::vector<double>& phisComputed)
   {
      this->phisComputed = phisComputed;
   }

   const std::vector<HEdge>& getReactionsInSolution() const
   {
      return reactionsInSolution;
   }

   void setReactionsInSolution(const std::vector<HEdge>& solution) {
      this->reactionsInSolution = solution;
   }

private:
   std::vector<HEdge> reactionsInSolution;
   std::vector<double> phisComputed;
   std::vector< pair<string, double>> deltasComputed;
};

inline const string Solution::getBriefSolutionString()
{
   string out;
   for( unsigned int i = 0; i < reactionsInSolution.size(); i++)
   {
      out += string(reactionsInSolution[i].getName()) + string(",");
   }
   return out;
}
#endif /* SRC_CPLEX_INTERFACE_SOLUTION_H_ */
