/* ***************************************************************************
 *
 *                              Totoro
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Ricardo Andrade
 *                  Irene Ziska
 *                  Alice Julien-Laferriere
 *                  Laurent Bulteau
 *                  Arnaud Mary
 *
 * ricardoluizandrade@gmail.com
 * irene.ziska@inria.fr
 * alice.julien.laferriere@gmail.com
 **
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#ifndef SRC_CPLEX_INTERFACE_CPLEX_MODELER_H_
#define SRC_CPLEX_INTERFACE_CPLEX_MODELER_H_

using namespace std;

#include <cmath>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <string>
#include <ilcplex/ilocplex.h>
#include <unordered_map>
#include <bits/unordered_map.h>
#include "../hypergraph/HGraph.h"
#include "Solution.h"


// ===========================================================================
//                             Definitions
// ===========================================================================
#define Optimal 2 // Return by CPLEX if solution is optimal

typedef IloArray<IloNumVarArray> IloNumVarMatrix;

typedef IloArray<IloNumVarMatrix> IloNumVarMatrix2;

// Int values for the alternative FBA mode
enum FBAMode
{
    minimise, maximise
};

class cplex_modeler
{
public:
    // =======================================================================
    //                               Constructors
    // =======================================================================
    cplex_modeler(const HGraph& graph, const double lambda, const double epsilon, const int timeout,
            const unsigned int maxIterations, const int ramMB, const int cpus);

    // ===========================================================================
    //                             Functions
    // ===========================================================================
    vector<Solution> solveModel();

    void setCreateIntermediaryFiles(bool f);

    void setIntermediaryFilesFolder(const std::string& folder);

    void setExchangeReactionsFile(const std::string& file);

    void setTuneSolver(bool t);

    void setReadParametersFile(bool r);

    void setParametersFileName(const std::string& filename);

    void setObejectiveFunctionWeights(double w1, double w2);

    void setMinimumAmountOfUsedReactions(int amount);

    void setMinimumAbsoluteVariationOfNonMeasuredCompounds(double variation);

    void setMaximumDegreeConstraintBound(unsigned int bound);

    vector<Solution> doFBAMinimizeAmountOfUsedReactions(const vector<HEdge> reactionsToProduce, const FBAMode mode);

protected:
    // =======================================================================
    //                             Protected Attributes
    // =======================================================================
    const IloEnv _env;

    IloModel _model;

    const HGraph _graph;

    const double _lambda = 1.0;

    const double _epsilon;

    const int _timeout;

    const int _ramMB;

    const int _cpus;

    bool _create_intermediary_files = false;

    std::string _intermediary_files_folder = "Sol/";


    bool _shoud_tune_solver = false;

    bool _should_read_parameters = false;

    std::string _parameters_file = "tunningParameters.txt";

    bool _should_fix_min_used_reactions = false;

    bool _should_fix_min_non_measured_compounds = false;

    int _min_used_reactions = 0;

    double _min_non_measured_compounds_abs_variation = 0.0;

    unsigned int _maxIterations = 0;

    unsigned int _maxDegreeConstraintBound = 0;

    // Big M values for the IFF constraint linking variables reactionIsUsed and fluxArray
    double _M_cut2 = 1e7;

    double _M_cut3 = 1e3;

    std::string _exchangeReactionsFilename;
    std::vector<std::string> exchangeReactions;

private:
    // Array of zeros and ones, used to initialize some bounds on the binary variables
    IloNumArray _zArray;

    IloNumArray _oArray;

    double maxAbsMeasuredVariation;

    IloCplex cplex;
    IloObjective objective;

    std::unordered_map<std::string, IloNumVar> phis;
    std::unordered_map<std::string, IloNumVar> deltas;
    std::unordered_map<std::string, IloRange> phiConstraints;

    std::unordered_map<std::string, IloBoolVar> binsIsUsed;
    std::unordered_map<std::string, IloRange> binaryConstraints;
//    std::unordered_map<std::string, IloIfThen> binaryConstraints;
    std::unordered_map<std::string, IloRange> maxDegreeConstraints;

    std::unordered_map<std::string, IloNumVar> absDeltas;
    std::unordered_map<std::string, IloRange> absDeltaConstraints;

    std::unordered_map<std::string, IloRange> excludeSolutionConstraints;

    IloRange minNumberUsedReaction;

    void computeMaxAbsoluteMeasuredVariation();

    void createPhisVars();
    void createDeltaVars();
    void createPhiConstraints();

    void createBinaries();
    void createBinaryConstraints();
    void createMaxDegreeConstraints();

    void createAbsDeltaVars();
    void createAbsDeltaConstraints();

    void createMinNumberUsedReactions();

    void createObjectiveFunction();

    void readParameters();
    void tuneSolver();

    Solution getSolutionValues();

    void excludeCurrentSolution(const Solution& solution, int counter);
};

inline void cplex_modeler::setCreateIntermediaryFiles(bool f)
{
    _create_intermediary_files = f;
}

inline void cplex_modeler::setIntermediaryFilesFolder(const std::string& folder)
{
    _intermediary_files_folder = folder;
}

inline void cplex_modeler::setTuneSolver(bool t)
{
    _shoud_tune_solver = t;
}

inline void cplex_modeler::setReadParametersFile(bool r)
{
    _should_read_parameters = r;
}

inline void cplex_modeler::setParametersFileName(const std::string& filename)
{
    _parameters_file = filename;
}

inline void cplex_modeler::setExchangeReactionsFile(const std::string& file)
{
    _exchangeReactionsFilename = file;

    std::vector<std::string> sbmls;

    std::ifstream inputStream(file);
    std::string line;

    if (inputStream.is_open())
    {
        while (std::getline(inputStream, line))
        {
            line.erase(std::remove(line.begin(), line.end(), '\t'), line.end());
            sbmls.emplace_back(line);
        }
    }
    else
    {
        std::cout << "Unable to open file for exchange reactioins: " << file << "!" << std::endl;
    }

    exchangeReactions = sbmls;
}

inline void cplex_modeler::setMinimumAmountOfUsedReactions(int amount)
{
    this->_should_fix_min_used_reactions = true;
    this->_min_used_reactions = amount;
}

inline void cplex_modeler::setMinimumAbsoluteVariationOfNonMeasuredCompounds(double variation)
{
    this->_should_fix_min_non_measured_compounds = true;
    this->_min_non_measured_compounds_abs_variation = variation;
}

inline void cplex_modeler::setMaximumDegreeConstraintBound(unsigned int bound)
{
    this->_maxDegreeConstraintBound = bound;
}

#endif /* SRC_CPLEX_INTERFACE_CPLEX_MODELER_H_ */
