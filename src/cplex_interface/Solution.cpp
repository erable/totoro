/* ***************************************************************************
 *
 *                              Totoro
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Ricardo Andrade
 *                  Irene Ziska
 *                  Alice Julien-Laferriere
 *                  Laurent Bulteau
 *                  Arnaud Mary
 *
 * ricardoluizandrade@gmail.com
 * irene.ziska@inria.fr
 * alice.julien.laferriere@gmail.com
 **
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "Solution.h"

Solution::Solution()
{
    this->reactionsInSolution = vector<HEdge>();
    this->phisComputed = vector<double>();
    this->deltasComputed = vector < pair<string , double>> () ;

}

Solution::~Solution()
{

}

void Solution::addEdgeToSolution(HEdge edge, double phis)
{
    this->reactionsInSolution.push_back(edge);
    this->phisComputed.push_back(phis);
}

void Solution::addNodeToSolution(const string name, const double delta )
{
    this->deltasComputed.push_back( pair<string, double> (name, delta));
}

// Solution size is the number of edge included
int Solution::size()
{
    return this->reactionsInSolution.size();
}


/*!
 * \brief Two files per solution written: phis and deltas
 * \param : id_sol : an  int that will increment the name of the solutions
 * \param folder: name of the folder where the files will be placed.
 * \param filename: beginning of the string naming the file for each solutions
 * Will write two file:
 *  - one file : 'filename + "Phis_"+ id_sol + ".txt" ', contains the Phis computed (tab separated file with 2 columns)
 *  reactionName \t PhisValue
 *  - one file : 'filename + "Deltas_" + id_sol + ".txt" ' with the computed Deltas
 * the information is extracted from the attributes of the Solution object (all attributes are used)
 */
const void Solution::writePhis_sol(const int id_sol, const char* folder, const char* filename)
{
    ofstream outputPhi;
    string namePhi =  string(folder) + string("/") + string(filename) + string("Phis_") + to_string( id_sol) + string(".txt");
    string nameDelta = string(folder) + string("/") +  string(filename) + string("Deltas_") + to_string( id_sol) + string(".txt");

    outputPhi.open(namePhi.c_str(), ios::out) ;
    if(outputPhi)
    {
        for (unsigned int iEdge = 0; iEdge <  reactionsInSolution.size(); ++iEdge ) // RMQ: solution size is the number of edges
        {
            outputPhi << reactionsInSolution[iEdge].getName() << " \t " << phisComputed[iEdge] <<endl;
        }
        outputPhi.close();
    }
    else
    {
        cerr << "Impossible to write the file for  phis values" << endl;
    }
    ofstream outputDeltas ;
    if (outputDeltas )
    {
    outputDeltas.open(nameDelta.c_str(), ios::out) ;
    for (auto iNodes : deltasComputed )
    {
        outputDeltas << iNodes.first << "\t" << iNodes.second << endl;
    }
    outputDeltas.close();
    }
    else
    {
        cerr << "Impossible to write the file for  deltas values" << endl;
    }

    cout << "[###]  writing files for phis and deltas... " << namePhi << " " << nameDelta << endl;
}


void Solution::display()
{
    if (reactionsInSolution.size() > 0)
    {
        for( unsigned int i = 0; i < reactionsInSolution.size(); i++)
        {
            cout << reactionsInSolution[i].getName() << ",";
        }
        std::cout << std::endl;
    }
    else
    {
        std::cout << "Empty solution." << std::endl;
    }
}
