/* ***************************************************************************
 *
 *                              Totoro
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Ricardo Andrade
 *                  Irene Ziska
 *                  Alice Julien-Laferriere
 *                  Laurent Bulteau
 *                  Arnaud Mary
 *
 * ricardoluizandrade@gmail.com
 * irene.ziska@inria.fr
 * alice.julien.laferriere@gmail.com
 **
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "read_writeFile.h"

using namespace std;

//! Reading xml and filling an HGraph Object
/*!
 * \brief read xml file and construct a  graph
 * \param fileName  name of the xml file
 * \param graph: HGraph object,
 * HGraph will be updates, a node will be created for every metabolites insides
 * the xml file.
 * Each reaction will be one edge
 */
void readXML (const char *fileName, HGraph & graph)
{
   string reactionName;
   string reactionDescription;
   list<string> products;
   list<double> productsStoich;
   list<string> reactants;
   list<double> reactantsStoich;
   string rev = "false";
   double flux_lower_bound = -100000.0;
   double flux_upper_bound = 100000.0;
   try
   {
      xmlpp::TextReader reader(fileName);
      string lastSpecies = "";
      while(reader.read())
      {
         string field = string(reader.get_name().c_str());
         //=========================================================
         // FILLING METABOLITES (SPECIES)
         //=========================================================
         string species = "";
         bool boundary = false;
         if( field.compare("species") == 0)
         {
            if (reader.has_attributes())
            {
               reader.move_to_first_attribute();
               do
               {
                  field = string(reader.get_name().c_str());
                  if(field.compare("id") == 0)
                  {
                     //HERE GET SPECIES!
                     species = string(reader.get_value().c_str());
                  }
                  else if (field.compare("boundaryCondition") == 0)
                  {
                     string sboundary = string(reader.get_value().c_str());
                     if (sboundary.compare("true") == 0)
                     {
                        boundary = true;
                     }
                  }
               } while (reader.move_to_next_attribute());
               reader.move_to_element();
               if(species.length() > 0 && species.compare(lastSpecies)!=0)
               {
                  graph.addNode(species,boundary);
                  lastSpecies = species;
                  species = "";
                  boundary = false;
               }
            }
            else
            {
               cout << "Nodes no ID!!" << endl;
            }

         }
         //=========================================================
         // Getting Reactions
         //=========================================================
         if (field.compare("reaction") == 0)
         {
            if (reactionName == "")
            {
               // GET REACTION ID! & name
               if (reader.has_attributes())
               {
                  reader.move_to_first_attribute();
                  do
                  {
                     field = string(reader.get_name().c_str());
                     if (field.compare("id") == 0)
                     {
                        reactionName =  string(reader.get_value().c_str());
                     }
                     if (field.compare("name") == 0)
                     {
                        reactionDescription = string(reader.get_value().c_str());
                     }
                     if (field.compare("reversible") == 0)
                     {
                        rev =  string(reader.get_value().c_str());
                        // convert rev to lower case
                        std::for_each(rev.begin(), rev.end(), [](char & c){
                           c = ::tolower(c);
                        });
                     }
                  } while (reader.move_to_next_attribute());
                  //reader.move_to_element();
               }
            }
            else
               //=========================================================
               // Writing Reactions to the graph
               //=========================================================
            {// Adding the reaction to graph
               field = string(reader.get_name().c_str());
               if (rev.compare("true") == 0)
               {
                  graph.addEdge("F_" + reactionName, reactionDescription, reactants, reactantsStoich, products,
                          productsStoich, flux_lower_bound, flux_upper_bound);
                  graph.addEdge("B_" +  reactionName, reactionDescription, products, productsStoich, reactants,
                          reactantsStoich, flux_lower_bound, flux_upper_bound);
               }
               else
               {
                  graph.addEdge("" + reactionName, reactionDescription, reactants, reactantsStoich, products, productsStoich, flux_lower_bound, flux_upper_bound);
               }

               products.clear();
               productsStoich.clear();
               reactants.clear();
               reactantsStoich.clear();
               reactionName= "";
               reactionDescription = "";
            }
         }
         // Finding reactants and products:
         bool producB;
         if (field.compare("listOfProducts") == 0)
         {
            producB = true;
         }
         if (field.compare("listOfReactants") == 0)
         {
            producB = false;
         }
         if (field.compare("speciesReference") == 0)
         {
            bool hasStoichiometry = false;
            if (reader.has_attributes())
            {
               reader.move_to_first_attribute();
               do
               {
                  field = string(reader.get_name().c_str());
                  if (field.compare("species") == 0)
                  {
                     if (producB)//products
                     {
                        products.push_front(string(reader.get_value().c_str()) );
                     }
                     else // reactant
                     {
                        reactants.push_front(string(reader.get_value().c_str()));
                     }
                  }
                  //reader.move_to_next_attribute();
                  //field = string(reader.get_name().c_str());
                  else if (field.compare("stoichiometry") == 0)
                  {
                     hasStoichiometry = true;
                     if (producB)//products
                     {
                        string st = string(reader.get_value().c_str());
                        double st_v = stod(st);
                        productsStoich.push_front(st_v);
                     }
                     else // reactant
                     {
                        string st = string(reader.get_value().c_str());
                        double st_v = stod(st);
                        reactantsStoich.push_front(st_v);
                     }
                  }
               } while (reader.move_to_next_attribute());
               reader.move_to_element();
               //field = string(reader.get_name().c_str());
            }
            if (!hasStoichiometry)
            {
               if (producB)//products
               {
                  productsStoich.push_front(1.0);
               }
               else // reactant
               {
                  reactantsStoich.push_front(1.0);
               }
            }
         }
         // Read the bounds for each reaction
         if (field.compare("parameter") == 0)
         {
            if (reader.has_attributes())
            {
               reader.move_to_first_attribute();
               do
               {
                  field = string(reader.get_name().c_str());
                  if (field.compare("id") == 0)
                  {
                     string type = string(reader.get_value().c_str());
                     if (type.compare("LOWER_BOUND") == 0)
                     {
                        reader.move_to_next_attribute();
                        string new_field = string(reader.get_name().c_str());
                        if (new_field.compare("value") == 0)
                        {
                           string flb = string(reader.get_value().c_str());
                           if ((flb.compare("INF") != 0) && (flb.compare("-INF") != 0))
                           {
                              flux_lower_bound = stod(flb);
                           }
                           else
                           {
                              flux_lower_bound = -100000.0;
                           }
                        }
                     }
                     else if (type.compare("UPPER_BOUND") == 0)
                     {
                        reader.move_to_next_attribute();
                        string new_field = string(reader.get_name().c_str());
                        if (new_field.compare("value") == 0)
                        {
                           string fub = string(reader.get_value().c_str());
                           if ((fub.compare("INF") != 0) && (fub.compare("-INF") != 0))
                           {
                              flux_upper_bound = stod(fub);
                           }
                           else
                           {
                              flux_upper_bound = 100000.0;
                           }
                        }
                     }
                  }
               }  while (reader.move_to_next_attribute());
            }
         }

      }
   }
   catch (const exception& e)
   {
      cout << "Exception caught: " << e.what() << endl;
   }
}




//We should be using special xml library but doc not coherent
//! Writting a pseudo- xml to visualize an and HGraph Object
/*!
 * \brief From a HGraph, wrtie an xml for visualisation
 * \param fileName  name of the xml file
 * \param graph: HGraph object,
 */
void writeXML (const char *fileName, HGraph & graph)
{
   FILE *writingXML = fopen (fileName, "w");
   // Header for conversion
   fprintf (writingXML, "<?xml version=\"1.0\"  encoding=\"UTF-8\"?>\n");
   fprintf (writingXML, "<sbml xmlns=\"http://www.sbml.org/sbml/level2\" version=\"1\" level=\"2\" xmlns:html=\"http://www.w3.org/1999/xhtml\">\n");
   fprintf (writingXML, "<model id=\"toyexample\" name=\"NA\">\n" );
   fprintf (writingXML," <listOfCompartments>\n");
   fprintf (writingXML, "<compartment id=\"NA\" name =\"NA\" /> \n ");
   fprintf (writingXML, "</listOfCompartments>\n");
   //=========== Species =================
   fprintf (writingXML, "<listOfSpecies>\n");
   map<int, HNode> compounds = graph.getNodes();

   for (auto it = compounds.begin(); it != compounds.end(); ++it)
   {
      int initialAmount = (it->second).isBlack()? 1 : 0;
      stringstream stmp;
      stmp << it->first;
      string idMet = string("__")+ stmp.str() + string("_met");
      fprintf (writingXML, "  <species id=\"%s\" name=\"%s\" compartment=\"NA\" boundaryCondition=\"false\"  initialAmount=\"%i\" />\n", (it->second).getName().c_str(),(it->second).getName().c_str(), initialAmount);
   }
   map<int,HEdge> allEdges = graph.getEdges();
   fprintf (writingXML, "</listOfSpecies>\n");
   //=========== Reactions =================
   fprintf (writingXML, "<listOfReactions>\n" );

   for (auto itHE = allEdges.begin(); itHE != allEdges.end(); ++itHE)
   {
      auto myEdge = (itHE->second);
      stringstream stmp;
      stmp << itHE->first;
      string idReac= string("__")+ stmp.str() + string("_reac");
      fprintf (writingXML,"  <reaction id=\"%s\" name=\"%s\" reversible=\"false\">\n",myEdge.getName().c_str(), myEdge.getName().c_str());
      //  the reactants

      fprintf (writingXML,"  <listOfReactants>\n");
      list <int> nodes = myEdge.getHeadNodes();
      for (auto itN=nodes.begin(); itN!=nodes.end(); ++itN)
      {
         stringstream stmp;
         stmp << *itN;
         string idMet = string("__") + stmp.str() + string("_met");
         fprintf (writingXML,"    <speciesReference species=\"%s\" stoichiometry=\"NaN\"/>\n", graph.getNode(*itN).getName().c_str());  // we dont have the stochiometry for now
      }
      fprintf (writingXML,"  </listOfReactants> \n" );
      //  the products
      fprintf (writingXML,"  <listOfProducts>\n");
      nodes = myEdge.getTailNodes();
      for (auto itN=nodes.begin(); itN!=nodes.end(); ++itN)
      {
         stringstream stmp;
         stmp << *itN;
         string toto =  string("__") + stmp.str() + string("_met");
         fprintf (writingXML,"    <speciesReference species=\"%s\" stoichiometry=\"NaN\"/>\n",  graph.getNode(*itN).getName().c_str() );
      }
      fprintf (writingXML,"  </listOfProducts>\n");
      fprintf (writingXML, "  </reaction>\n");
   }
   fprintf (writingXML, "</listOfReactions>\n" );
   fprintf (writingXML, "</model>\n");

   fprintf (writingXML,"</sbml>");

   fclose (writingXML);

}


//! Reading bn File
/*!
 * \brief read file of black nodes and updates the graph
 * \param fileName  name of the file
 * \param graph: HGraph object,
 */
void readBNfile (const char *fileName, HGraph & graph)
{
   FILE *readBn = fopen (fileName, "r");
   if (readBn == NULL)
   {
      cerr << "Error opening the black node file" << endl;
      exit(EXIT_FAILURE);
   }
   char *line= new char[1000];
   char *compound;// length: random choose here, can be increased
   char *bmin = NULL;
   char *bmax = NULL;

   while (fgets(line, 1000, readBn) != NULL)
   {
      compound = strtok( line, " \t" );
      bmin = strtok( NULL, " \t" );
      bmax = strtok( NULL, "\n" );
      assert(bmin != NULL);
      assert(bmax != NULL);
      double bdoublevaluemin = atof(bmin);
      double bdoublevaluemax = atof(bmax);

      graph.setNodeBlack(compound,bdoublevaluemin,bdoublevaluemax);
   }
   fclose(readBn);
}

/**
 * Read a text file in which each line has a reaction ID, and two real values,
 * a lower and a upper bound for the flux of that reaction.
 * Overides the bounds in the HGraph with the values from the file.
 * \param fileName  name of the file to be read
 * \param graph: HGraph object.
 */
void readReactionBoundsFile (const char *fileName, HGraph & graph)
{
   FILE *readBn = fopen (fileName, "r");
   if (readBn == NULL)
   {
      std::cerr << "Error opening the black node file" << std::endl;
      exit(EXIT_FAILURE);
   }
   char *line= new char[1000];
   char *name;// length: random choose here, can be increased
   char *bmin = NULL;
   char *bmax = NULL;

   while (fgets(line, 1000, readBn) != NULL)
   {
      name = strtok( line, " \t" );
      bmin = strtok( NULL, " \t" );
      bmax = strtok( NULL, "\n" );
      assert(bmin != NULL);
      assert(bmax != NULL);
      double bdoublevaluemin = atof(bmin);
      double bdoublevaluemax = atof(bmax);
      if (bdoublevaluemin > bdoublevaluemax){
         double t;
         t = bdoublevaluemax;
         bdoublevaluemax = bdoublevaluemin;
         bdoublevaluemin = t;
      }

      boost::optional<HEdge> e = graph.getEdge(name);
      if (e != boost::none)
      {
         e->setFluxLowerBound(bdoublevaluemin);
         e->setFluxUpperBound(bdoublevaluemax);
      }
   }
   fclose(readBn);
}

/**
 * Read a text file in which each line has a compound (species) ID
 * and filter those compounds from the graph, taking them out of
 * each reaction.
 * \param fileName  name of the file to be read
 * \param graph: HGraph object.
 */
void filterCofactorsFromFile (const char *fileName, HGraph & graph)
{
   FILE *cofactorsFile = fopen (fileName, "r");
   if (cofactorsFile == NULL)
   {
      std::cerr << "Error opening the cofactors id file" << std::endl;
      exit(EXIT_FAILURE);
   }
   char *line= new char[1000];
   char *name;// length: random choose here, can be increased

   while (fgets(line, 1000, cofactorsFile) != NULL)
   {
      name = strtok( line, " \n" );
      boost::optional<HNode> n = graph.getNode(name);
      if (n != boost::none)
      {
         graph.removeNode(n->getId());
      }
   }
   fclose(cofactorsFile);
}
