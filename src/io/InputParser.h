/* ***************************************************************************
 *
 *                              Totoro
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Ricardo Andrade
 *                  Irene Ziska
 *                  Alice Julien-Laferriere
 *                  Laurent Bulteau
 *                  Arnaud Mary
 *
 * ricardoluizandrade@gmail.com
 * irene.ziska@inria.fr
 * alice.julien.laferriere@gmail.com
 **
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#ifndef SRC_IO_INPUTPARSER_H_
#define SRC_IO_INPUTPARSER_H_

#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>
#include <iostream>
#include <string>

class CmdLineParser {
public:
   CmdLineParser();
   void parseInput(int argc, char** argv);
   const std::string& getDeltaFileName() const;
   const std::string& getNetworkFileName() const;
   const std::string& getOutputFileName() const;
   const std::string& getBoundsFilename() const;
   const std::string& getFolderIntermediateFiles() const;
   const std::string& getSolverParametersFileName() const;
   const std::string& getExchangeReactionsFilename() const;
   const bool shouldParseExchangeReactions() const;
   const bool shouldFixAmountOfUsedReactions() const;
   const bool shouldFixAbsVariationOfNonMeasuredCompounds() const;
   const bool shouldCreateIntermediateFiles() const;
   const bool shouldReadSolverParametersFile() const;
   const bool shouldReadBoundsFile() const;
   const bool shouldTuneSolver() const;
   const int& getOptimizerTimeout() const;
   const int& getOptimizerInternalMemory() const;
   const int& getOptimizerCPUAmount() const;
   const double& getLambda() const;
   const double& getDeltaEpsilon() const;
   const double& getAbsVariationOfNonMeasuredCompounds() const;
   const int& getDegreeConstraintBound() const;
   const int& getAmountOfUsedReactions() const;
   const int& getMaxEnumerationIterations() const;
   const std::vector<std::string>& getReactionIdsToMaximise() const;
   const std::vector<std::string>& getReactionIdsToMinimise() const;
   const std::string& getCofactorsIdListFileName() const;
   const bool shouldFilterCofactors() const;


   const bool didAskedForHelp() const;
   void printHelpDescription() const;

   virtual ~CmdLineParser();

private:
   boost::program_options::options_description _desc;
   bool askedForHelp = false;
   bool _intermediate_files = false;
   bool _tuning_mode = false;
   bool _read_solver_parameters = false;
   bool _shouldFixAbsVariationOfNoneasuredCompounds = false;
   bool _shouldFixAmountOfUsedReactions = false;
   bool _read_bounds_file = false;
   bool _read_exchange_reactions_file = false;
   bool _read_expected_file = false;
   bool _filter_cofactors = false;
   std::string _delta_file;
   std::string _network_filename;
   std::string _bounds_filename = "";
   std::string _output_filename;
   std::string _intermediate_folder;
   std::string _exchange_reactions_filename;
   std::string _tuning_output_file = "tuningParameters.txt";
   std::string _cofactorsids_filename = "";
   int _optTimeout;
   int _optMemory;
   int _optCPUs;
   int _maxEnumIterations = 0;
   int _degreeConstraintBound = 0;
   double _deltaEpsilon;
   double _lambda;
   int _fixedAmountOfUsedReactions = -1;
   double _fixedAbsVariationOfNonMeasuredCompounds = -1.0;

   std::vector<std::string> _reactions_to_maximise;
   std::vector<std::string> _reactions_to_minimise;

};
inline const bool CmdLineParser::didAskedForHelp() const
{
   return askedForHelp;
}

inline const bool CmdLineParser::shouldFixAmountOfUsedReactions() const
{
   return _shouldFixAmountOfUsedReactions;
}

inline const bool CmdLineParser::shouldFixAbsVariationOfNonMeasuredCompounds() const
{
   return _shouldFixAbsVariationOfNoneasuredCompounds;
}

inline const std::string& CmdLineParser::getDeltaFileName() const
{
   return _delta_file;
}

inline const std::string& CmdLineParser::getNetworkFileName() const
{
   return _network_filename;
}

inline const std::string& CmdLineParser::getOutputFileName() const
{
   return _output_filename;
}

inline const std::string& CmdLineParser::getBoundsFilename() const
{
   return _bounds_filename;
}

inline const std::string& CmdLineParser::getExchangeReactionsFilename() const
{
   return _exchange_reactions_filename;
}

inline const bool CmdLineParser::shouldParseExchangeReactions() const
{
   return _read_exchange_reactions_file;
}

inline const bool CmdLineParser::shouldFilterCofactors() const
{
   return _filter_cofactors;
}

inline const std::string& CmdLineParser::getCofactorsIdListFileName() const
{
   return _cofactorsids_filename;
}

inline const std::string& CmdLineParser::getSolverParametersFileName() const
{
   return _tuning_output_file;
}

inline const std::string& CmdLineParser::getFolderIntermediateFiles() const
{
   return _intermediate_folder;
}

inline const bool CmdLineParser::shouldCreateIntermediateFiles() const
{
   return _intermediate_files;
}

inline const bool CmdLineParser::shouldReadSolverParametersFile() const
{
   return _read_solver_parameters;
}

inline const bool CmdLineParser::shouldReadBoundsFile() const
{
   return _read_bounds_file;
}

inline const bool CmdLineParser::shouldTuneSolver() const
{
   return _tuning_mode;
}
inline const int& CmdLineParser::getOptimizerTimeout() const
{
   return _optTimeout;
}

inline const int& CmdLineParser::getOptimizerInternalMemory() const
{
   return _optMemory;
}

inline const int& CmdLineParser::getOptimizerCPUAmount() const
{
   return _optCPUs;
}

inline const int& CmdLineParser::getMaxEnumerationIterations() const
{
   return _maxEnumIterations;
}

inline const double& CmdLineParser::getLambda() const
{
   return _lambda;
}

inline const double& CmdLineParser::getDeltaEpsilon() const
{
   return _deltaEpsilon;
}

inline const double& CmdLineParser::getAbsVariationOfNonMeasuredCompounds() const
{
   return _fixedAbsVariationOfNonMeasuredCompounds;
}

inline const int& CmdLineParser::getAmountOfUsedReactions() const
{
   return _fixedAmountOfUsedReactions;
}

inline const int& CmdLineParser::getDegreeConstraintBound() const
{
   return _degreeConstraintBound;
}
inline const std::vector<std::string>& CmdLineParser::getReactionIdsToMaximise() const
{
   return _reactions_to_maximise;
}

inline const std::vector<std::string>& CmdLineParser::getReactionIdsToMinimise() const
{
   return _reactions_to_minimise;
}

inline void CmdLineParser::printHelpDescription() const
{
      //_desc.print(std::cout);
      std::cout << _desc << std::endl;
}

#endif /* SRC_IO_INPUTPARSER_H_ */
