/* ***************************************************************************
 *
 *                              Totoro
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Ricardo Andrade
 *                  Irene Ziska
 *                  Alice Julien-Laferriere
 *                  Laurent Bulteau
 *                  Arnaud Mary
 *
 * ricardoluizandrade@gmail.com
 * irene.ziska@inria.fr
 * alice.julien.laferriere@gmail.com
 **
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#include "InputParser.h"
#include <vector>

CmdLineParser::CmdLineParser() : _desc("Allowed options")
{
   //boost::program_options::options_description &dsc("Allowed options");
   //_desc = dsc;
   _desc.add_options()
     ("help,h", "produce help message")
     ("network-file,N", boost::program_options ::value<std::string>(&_network_filename), "input network file (MANDATORY).")
     ("delta-file,D", boost::program_options ::value<std::string>(&_delta_file), "input file with the measured variations (MANDATORY).")
     ("bounds-file,b", boost::program_options ::value<std::string>(&_bounds_filename), "input file with the lower and upper bounds for each reaction id, it is an option to override the bounds provided on the SBML file.")
     ("output-file,o", boost::program_options ::value<std::string>(&_output_filename)->default_value("solutions.txt"), "output file.")
     ("create-intermediate-output-files", "create intermediate output files.")
     ("intermediates-output-files-folder", boost::program_options ::value<std::string>(&_intermediate_folder)->default_value("Sol/"), "Specify the folder for the intermediate output files.")
     ("exchange-reactions-file", boost::program_options ::value<std::string>(&_exchange_reactions_filename), "exchange reactions file.")
     ("cofactors-ids-file", boost::program_options ::value<std::string>(&_cofactorsids_filename), "input file with a list of compounds (species) ids to be filtered out from the network in order to avoid unnatural paths.")
     ("lambda,l", boost::program_options ::value<double>(&_lambda)->default_value(1.0), "Weight used in the objective function (lambda*{Sum of used reactions} + (1-lambda)*{Sum of concentrations(absolute) of the non-measured compounds}. Should be a real number in the interval [0.0,1.0].")
     ("fix-nm-variations,v", boost::program_options ::value<double>(), "Fix the value for the absolute variation of the non-measured compounds.")
     ("fix-amount-reactions,r", boost::program_options ::value<int>(), "Fix the amount of reactions present in a story.")
     ("input-epsilon,e", boost::program_options ::value<double>(&_deltaEpsilon)->default_value(0.30), "Epsilon for the non-measured deltas.")
     ("degree-constraint", boost::program_options ::value<int>(&_degreeConstraintBound)->default_value(0), "And optional extra constraint that puts an upper bound in the degree of a node in a story.")
     ("max-numeration-iterations", boost::program_options ::value<int>(&_maxEnumIterations)->default_value(0), "Maximum number of solutions. If during enumeration more solutions than this are found, the method stops and return all found solutions. Zero (0), default, means no limit. ")
     ("optimizer-timeout,t", boost::program_options ::value<int>(&_optTimeout)->default_value(3600), "Timeout for the optimization procedure (in seconds).")
     ("optimizer-internal-memory,m", boost::program_options ::value<int>(&_optMemory)->default_value(2048), "Internal memory used by the optimizer before using disk space (in Mb).")
     ("optimizer-cpus,c", boost::program_options ::value<int>(&_optCPUs)->default_value(0), "Amount of threads used by the optimizer in parallel mode.")
     ("tune-solver", boost::program_options::value<std::string>(), "Alternate mode where no story is obtained.\n Instead, tune the internal solver for the specified model data and stores the parameters in the specified file.")
     ("load-solver-parameters,p", boost::program_options::value<std::string>(), "Sets the internal solver parameters according to the specified file.")
     ("maximise", boost::program_options::value<std::string>(), "[Alternative mode] FBA mode, in this mode user provide a list of reactions to maximize and the solver returns a flux distribution that maximises it in steady state if it exists.");

}

CmdLineParser::~CmdLineParser()
{
   // TODO Auto-generated destructor stub
}

void CmdLineParser::parseInput(int argc, char** argv )
{
   boost::program_options::variables_map vm;
   boost::program_options::store(boost::program_options::parse_command_line(argc, argv, _desc), vm);
   boost::program_options::notify(vm);
   if (vm.count("help")) {
      this->askedForHelp = true;
   }
   if (vm.count("create-intermediate-output-files")) {
      this->_intermediate_files = true;
   }
   if (vm.count("output-file"))
   {
      this->_output_filename = vm["output-file"].as<std::string>();
   }
   if (vm.count("delta-file"))
   {
      this->_delta_file = vm["delta-file"].as<std::string>();
   }
   if (vm.count("network-file"))
   {
      this->_network_filename = vm["network-file"].as<std::string>();
   }
   if (vm.count("bounds-file"))
   {
      this->_bounds_filename = vm["bounds-file"].as<std::string>();
      _read_bounds_file = true;
   }
   if (vm.count("exchange-reactions-file"))
   {
      this->_exchange_reactions_filename = vm["exchange-reactions-file"].as<std::string>();
      _read_exchange_reactions_file = true;
   }
   if (vm.count("cofactors-ids-file"))
   {
      this->_cofactorsids_filename = vm["cofactors-ids-file"].as<std::string>();
      _filter_cofactors = true;
   }
   if (vm.count("tune-solver"))
   {
      this->_tuning_output_file = vm["tune-solver"].as<std::string>();
      _tuning_mode = true;
   }
   if (vm.count("load-solver-parameters"))
   {
      this->_tuning_output_file = vm["load-solver-parameters"].as<std::string>();
      _read_solver_parameters = true;
   }
   if (vm.count("intermediates-output-files-folder"))
   {
      this->_intermediate_folder = vm["intermediates-output-files-folder"].as<std::string>();
   }
   if (vm.count("optimizer-cpus"))
   {
      this->_optCPUs = vm["optimizer-cpus"].as<int>();
   }
   if (vm.count("max-numeration-iterations"))
   {
      this->_maxEnumIterations = vm["max-numeration-iterations"].as<int>();
   }
   if (vm.count("degree-constraint"))
   {
      this->_degreeConstraintBound = vm["degree-constraint"].as<int>();
   }
   if (vm.count("optimizer-timeout"))
   {
      this->_optTimeout = vm["optimizer-timeout"].as<int>();
   }
   if (vm.count("optimizer-internal-memory"))
   {
      this->_optMemory = vm["optimizer-internal-memory"].as<int>();
   }
   if (vm.count("lambda"))
   {
      this->_lambda = vm["lambda"].as<double>();
      if (_lambda < 0.0 || _lambda > 1.0)
      {
         throw std::invalid_argument( "Received lambda outside legal bounds ( [0,1] )." );
      }
   }
   if (vm.count("input-epsilon"))
   {
      this->_deltaEpsilon = vm["input-epsilon"].as<double>();
   }
   if (vm.count("fix-nm-variations"))
   {
      this->_fixedAbsVariationOfNonMeasuredCompounds = vm["fix-nm-variations"].as<double>();
      this->_shouldFixAbsVariationOfNoneasuredCompounds = true;
      if (_fixedAbsVariationOfNonMeasuredCompounds < 0)
      {
         throw std::invalid_argument( "Received negative value for the absolute sum of measured compounds." );
      }
   }
   if (vm.count("fix-amount-reactions"))
   {
      this->_fixedAmountOfUsedReactions = vm["fix-amount-reactions"].as<int>();
      this->_shouldFixAmountOfUsedReactions = true;
      if (_fixedAmountOfUsedReactions < 0)
      {
         throw std::invalid_argument( "Received negative value for the sum of used reactions." );
      }
   }

   if (vm.count("maximise"))
   {
      std::string  reactions_ids = vm["maximise"].as<std::string>();
      boost::char_separator<char> sep(", ");
      boost::tokenizer<boost::char_separator<char>> tokens(reactions_ids, sep);
      for (const auto& t : tokens) {
         this->_reactions_to_maximise.push_back(t);
      }
   }
}
