/* ***************************************************************************
 *
 *                              Totoro
 *
 * ***************************************************************************
 *
 * Copyright INRIA
 *  contributors :  Ricardo Andrade
 *                  Irene Ziska
 *                  Alice Julien-Laferriere
 *                  Laurent Bulteau
 *                  Arnaud Mary
 *
 * ricardoluizandrade@gmail.com
 * irene.ziska@inria.fr
 * alice.julien.laferriere@gmail.com
 **
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// ===========================================================================
//                               Include Libraries
// ===========================================================================



// ===========================================================================
//                             Include Project Files
// ===========================================================================
#include "Decomposition.h"



// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================




// ===========================================================================
//                             Functions
// ===========================================================================
//!Will explore the graph
/*! Depth-first search recursive
 *��\brief Explore all nodes reachable from v
 * @param h :  hypergraph being traverse
 * @param visited : list
 * @param v
 */
void explore (HGraph & h,  vector<bool> & visitedN, int v, vector<bool> & followedE) // watchout decay
{

   visitedN[v] = true;
   //=== Try OUt Edge
   list<int> outEdges = h.getNode(v).getOutEdges();
   for(auto it = outEdges.begin(); it != outEdges.end(); ++it)
   {
      if( ! followedE[*it] ) // we did not handled this edge yet
      {
         followedE.at(*it) = true;
         HEdge beingUsed = h.getEdge(*it);
         // visiting
         list<int> headNodes = beingUsed.getHeadNodes();
         for(auto itNHead = headNodes.begin(); itNHead != headNodes.end(); ++itNHead)
         {
            if(! visitedN[*itNHead] )
            {
               explore(h, visitedN, *itNHead, followedE); // try to explore from h
            }
         }
         list<int> tailNodes = beingUsed.getTailNodes();
         for(auto itNTail = tailNodes.begin(); itNTail != tailNodes.end(); ++itNTail)
         {
            if( !  visitedN[*itNTail] )
            {
               explore( h, visitedN, *itNTail, followedE);
            }
         }
      }
   }
   //=== Try IN edges
   list<int> inEdges = h.getNode(v).getInEdges();
   for(auto it = inEdges.begin(); it != inEdges.end(); ++it)
   {
      if( ! followedE[*it] ) // we did not handled this edge yet
      {
         followedE.at(*it) = true;
         HEdge beingUsed = h.getEdge(*it);
         // visiting
         list<int> headNodes = beingUsed.getHeadNodes();
         for(auto itNHead = headNodes.begin(); itNHead != headNodes.end(); ++itNHead)
         {
            if(! visitedN[*itNHead] )
            {
               explore( h , visitedN, *itNHead, followedE); // try to explore from h
            }
         }
         list<int> tailNodes = beingUsed.getTailNodes();
         for(auto itNTail = tailNodes.begin(); itNTail != tailNodes.end(); ++itNTail)
         {
            if( !  visitedN[*itNTail] )
            {
               explore( h, visitedN, *itNTail, followedE);
            }
         }
      }
   }
}


//!Extract subgraph
/*!
 *\brief Decompose h into its connected components using a dfs algorithm
 *\param h : the hypergraph to decompose
 *\return the list of modules including only blackNodes
 */
list<HGraph> dfsDecomposition (HGraph &  h)
                  {
   list <HGraph> connectedParts;
   while (! (h.getBlackNodes().empty())) // all bn are in a component
   {
      //======= initializing
      int nbNodes = h.getNbNodes();
      vector<bool> visited (nbNodes, false);

      int nbEdges = h.getNbEdges();
      vector<bool> followedE (nbEdges, false);

      // end initialisation
      std::cout << "i";
      //===========do dfs
      explore(h, visited, h.getBlackNodes().front(), followedE );
      cout << "finished explore!"<< endl;
      //=========== extract sub & modify h
      connectedParts.push_front( extractSub(h, visited)) ;
   }
   return connectedParts;
                  }


// From h, remove nodes where visited is true, and create a new one with only those ones.
HGraph extractSub( HGraph & h, vector<bool> &  visited)
{
   int nbNodes = h.getNbNodes();
   HGraph newH = HGraph(h);
   // Updating h
   for(int i = 0; i != nbNodes; i++ )
   {
      if(visited[i])
      {
         h.removeNode(i);
      }
      else
      {
         newH.removeNode(i);
      }
   }
   h.reIndexNodes();
   newH.reIndexNodes();
   return newH;
}

