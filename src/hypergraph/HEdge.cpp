/*
 * HEdge.cpp
 *
 *  Created on: 30 juin 2014
 *      Author: ajulien
 */

// ===========================================================================
//                               Include Libraries
// ===========================================================================
#include "HEdge.h"

#include <string>
#include <sstream>
#include <iostream>
#include <cassert> // remove if #define NFDEBUG

// ===========================================================================
//                             Include Project Files
// ===========================================================================

// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================

//############################################################################
//                                                                           #
//                               Class HEdge                                 #
//                                                                           #
//############################################################################

// ===========================================================================
//                               Static attributes
// ===========================================================================

// ===========================================================================
//                                  Constructors
// ===========================================================================
HEdge::HEdge( const string nameReactions, const string description, const int idRe, const double& fluxLB, const double& fluxUB ) : id(idRe), lower_bound(fluxLB), upper_bound(fluxUB)
{
   name =  nameReactions;
   desc = description;

}

HEdge::HEdge( const string & nameReactions, const string& description, const  list <int>&  _nodesIdOut, const list <double>&  _nodesStoichOut, const list <int> & _nodesIdIn, const list <double>&  _nodesStoichIn, const int & idRe, const double & fluxLB, const double& fluxUB ) : id(idRe), lower_bound(fluxLB), upper_bound(fluxUB)
{
   name =  nameReactions;
   desc = description;
   nodesTail = _nodesIdOut;
   stoichTail = _nodesStoichOut;
   nodesHead = _nodesIdIn;
   stoichHead = _nodesStoichIn;
}

HEdge::HEdge( const HEdge &model ) : id(model.id),lower_bound(model.lower_bound), upper_bound(model.upper_bound)
{
   // cout << "in copy HEDGE";
   name = model.name;
   desc = model.desc;
   //!Ids of the out going nodes
   nodesTail = model.nodesTail;//
   //!Ids of the in going nodes
   nodesHead = model.nodesHead;
   stoichHead = model.stoichHead;
   stoichTail = model.stoichTail;
}
// ===========================================================================
//                                  Destructors
// ===========================================================================
HEdge::~HEdge( void )
{

}
// ===========================================================================
//                                   Operators
// ===========================================================================

bool HEdge::operator==(const HEdge & a) const
{
   return (id == a.id);
}

bool HEdge::operator<(const HEdge & a) const
{
   return ( id < a.id);
}


HEdge& HEdge::operator=(const HEdge& other)
{
   HEdge* cobj = new HEdge(other.getName(), other.getDescription(), other.getHeadNodes(),other.getHeadStoichiometry(),other.getTailNodes(),other.getTailStoichiometry(),other.getId(),other.getFluxLowerBound(),other.getFluxUpperBound());
   return *cobj;
}

// ===========================================================================
//                                 Public Methods
// ===========================================================================
void HEdge::display (void) const
{
   cout << "\t " << "id: " << id << " name :"<< name << ":\t" ;
   for(auto it = nodesTail.begin(); it != nodesTail.end(); it++ )
   {
      cout << *it << " " ;
   }
   cout << " -> " ;
   for(auto it = nodesHead.begin(); it != nodesHead.end(); it++ )
   {
      cout << *it << " ";
   }
}

string HEdge::to_str (void) const
{
   string return_str = "";
   stringstream sid;
   sid << id;
   return_str ="\t id: " + sid.str() +" name :" + name + ":\t" ;
   for(auto it = nodesTail.begin(); it != nodesTail.end(); it++ )
   {
      stringstream ss;
      ss << *it;
      return_str += ss.str() + " " ;
   }
   return_str += " -> " ;
   for(auto it = nodesHead.begin(); it != nodesHead.end(); it++ )
   {
      stringstream ss;
      ss << *it;
      return_str += ss.str() + " ";
   }

   return return_str;
}
// ===========================================================================
//                                Protected Methods
// ===========================================================================

// ===========================================================================
//                              Non inline accessors
// ===========================================================================
int HEdge::getTailSize (void) const
{
   return nodesTail.size();
}
int HEdge::getHeadSize (void) const
{
   return nodesHead.size();
}

void HEdge::addTailNode (int newNode, double stoichiometry)
{
   nodesTail.push_front (newNode);
   stoichTail.push_front(stoichiometry);
}

void HEdge::addHeadNode (int newNode, double stoichiometry)
{
   nodesHead.push_front (newNode);
   stoichHead.push_front(stoichiometry);
}

void HEdge::removeTailNode (int toRemoveNode)
{
   list<int>::iterator itCompound;
   list<double>::iterator itStoich;
   for (itCompound = nodesTail.begin(), itStoich = stoichTail.begin(); (itCompound != nodesTail.end()) && (itStoich != stoichTail.end()); ++itCompound, ++itStoich )
   {
      if(*itCompound == toRemoveNode)
      {
         nodesTail.erase(itCompound);
         stoichTail.erase(itStoich);
         break;
      }
   }
}

void HEdge::removeHeadNode (int toRemoveNode)
{
   list<int>::iterator itCompound;
   list<double>::iterator itStoich;
   for (itCompound = nodesHead.begin(), itStoich = stoichHead.begin(); (itCompound != nodesHead.end()) && (itStoich != stoichHead.end()); ++itCompound, ++itStoich )
   {
      if(*itCompound == toRemoveNode)
      {
         nodesHead.erase(itCompound);
         stoichHead.erase(itStoich);
         break;
      }
   }
}



